package ekzos.authenticationservice;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Pawel on 2015-01-17.
 */
@Controller
@RequestMapping("/login")
public class LoginController {

    // kontroler widoku strony przed logowaniem
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showMainPage() {
        ModelAndView model = new ModelAndView("EkzosWelcome");
        return model; // zwraca widok strony z polem do logowania
    }



}
