package ekzos.authenticationservice;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Pawel on 2015-01-17.
 */
@Controller
@RequestMapping(value = "/student")
public class StudentController {


    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView studentIndexPage(){
        return new ModelAndView("dashboard_student");
    }
}
