<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="http://getbootstrap.com/favicon.ico">

<title>EKZOS - Logowanie</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/signin.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="index_files/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>

    <body>
        
      <div class="container">
      <div class="row">
          <div class="col-md-offset-2 col-md-8 jumbotron text-center">
            <h1>EKZOS</h1>
            <div class="lead">Tw�j elektroniczny system studenta</div>
            <div class="lead">Logowanie</div>
            <form class="form-signin"  action="<c:url value='j_spring_security_check' />" method="POST">
              
              <label for="inputEmail" class="sr-only" >Email address</label>
              
              <input id="inputEmail" name="username" class="form-control" placeholder="Email address" autofocus="" type="email">
              
              <label for="inputPassword" class="sr-only">Password</label>
              
              <input id="inputPassword" class="form-control" placeholder="Password" name="password" type="password">

                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Sign in" />

              <br/>
                <input type="hidden" name="${_csrf.parameterName}"
                       value="${_csrf.token}" />
              <button class="btn btn-info btn-block">Rejestracja</button>
            </form>
          </div>
        </div>
      </div> <!-- /container -->


      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="index_files/ie10-viewport-bug-workaround.js"></script>


    </body></html>