<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
  <meta charset="utf-8"/>
  <title>EKZOS - Twoje studenckie konto</title>
  <meta name="generator" content="Bootply" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
  <link href="css/bootstrap.css" rel="stylesheet"/>
  <link href="css/styles.css" rel="stylesheet"/>
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
      <link href="css/styles.css" rel="stylesheet"/>
    </head>
    <body>
      <!-- Header -->
      
      <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">

          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">EKZOS</a>
          </div>
          <!--menu-->
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
             <li><a href="#">Studenci</a></li>
             <li><a href="#">Operacje</a></li>
           </ul>
           <ul class="nav navbar-nav navbar-right">
              <li class="active">
                <a>
                 pracownik Jan Kowalski, #123654
               </a>
             </li>
             <li><a href="#">Ustawienia</a></li>
             <li><a href="#">Wyloguj</a></li>
           </ul>
         </div><!--/.nav-collapse -->

       </div>

     </div>
   </div>
   <!-- /Header -->

   <!-- Main -->
   <div class="container">
     <div class="row">
       <div class="col-md-4">
        <!-- students -->
        <h2>Powiadomienia</h2>
        <!-- notifications -->
        <div >

          <div class="panel panel-default top-margin" >
            <div class="panel-body">
              <div class="list-group">

                <a class="list-group-item active" href="#">

                  Lorem profile dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 

                </a>
                <a class="list-group-item" href="#">

                  Lorem profile dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 

                </a>
                <a class="list-group-item" href="#">

                  Lorem profile dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 

                </a>
                <a class="list-group-item" href="#">

                  Lorem profile dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 

                </a> 
              </div>
            </div>
          </div>
        </div>
        <!-- notifications end -->
      </div>
      <!-- students end -->


      <!-- operations -->
      <div class="col-md-8">
        <h2>Ostatnie operacje</h2>
        <table class="table table-striped">
          <thead>
            <tr>
              <th class="col-md-2">Data</th>
              <th class="col-md-3">Data zapłaty</th>
              <th class="col-md-2">Kwota</th>
              <th>Tytuł</th>
              <th class="col-md-1">Akcja</th>
            </tr>
          </thead>
          <tbody>
            <tr class="danger">
              <td>12/09/2012</td>
              <td>10/09/2012</td>
              <td>-1234 zł</td>
              <td>
                Sam sapien massa, aliquam 
              </td>
              <td>
                <button class="btn">Więcej</button>
              </td>
            </tr>
            <tr class="warning">
              <td>12/09/2012</td>
              <td>13/09/2012</td>
              <td>-1234 zł</td>
              <td>
                After RWD massa, aliquam
              </td>
              <td>
                <button class="btn">Więcej</button>
              </td>
            </tr>
            <tr>
              <td>98</td>
              <td>25%</td>
              <td>Type</td>
              <td>
                Wil sapien massa, 
              </td>
              <td>
                <button class="btn">Więcej</button>
              </td>
            </tr>
            <tr>
              <td>109</td>
              <td>8%</td>
              <td>..</td>
              <td>
                Forfoot aliquam in
              </td>
              <td>
                <button class="btn disabled">Więcej</button>
              </td>
            </tr>
            <tr>
              <td>34</td>
              <td>14%</td>
              <td>..</td>
              <td>
                Mikel sapien massa,
              </td>
              <td>
                <button class="btn">Więcej</button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- operations end -->           
    </div>

  </div>


</div><!--/container-->
<!-- /Main -->


<footer class="text-center">This Bootstrap 3 dashboard layout is compliments of <a href="http://www.bootply.com/85850"><strong>Bootply.com</strong></a></footer>


<div class="modal" id="addWidgetModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Add Widget</h4>
      </div>
      <div class="modal-body">
        <p>Add a widget stuff here..</p>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn">Close</a>
        <a href="#" class="btn btn-primary">Save changes</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dalog -->
</div><!-- /.modal -->




<!-- script references -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>