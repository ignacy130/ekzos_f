<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
  <meta charset="utf-8"/>
  <title>EKZOS - Wyślij</title>
  <meta name="generator" content="Bootply" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
  <link href="css/bootstrap.css" rel="stylesheet"/>
  <link href="css/styles.css" rel="stylesheet"/>
    <!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
      <link href="css/styles.css" rel="stylesheet"/>
    </head>
    <body>
      <!-- Header -->
      <div id="top-nav" class="navbar navbar-inverse navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-toggle"></span>
            </button>
            <ul class="nav navbar-nav navbar-left">
              <li class="active">
                <a class="navbar-brand " href="#">EKZOS - Twoje studenckie konto</a>
              </li>
            </ul>
          </div>
          
          <ul class="nav navbar-nav navbar-right">
            <li class="active">
              <a >
               Jan Kowalski, #123654
             </a>
           </li>
           <li><a href="settings.htm">Ustawienia</a></li>
           <li><a href="#">Wyloguj</a></li>

         </ul>


       </div><!-- /container -->
     </div>
     <!-- /Header -->
   </div>
   <!-- /span-3 -->
   <!-- user menu end -->

   <div class="container">
    <div class="row">
      <h2>Ustawienia</h2>
      <h4>Możesz tu zmienić swój email, hasło, numer konta oraz adres</h4>
      <div class="col-md-5">
        <h4>Dane uczelniane</h4>
        <form>

          <label for="inputName" class="sr-only">Imię i nazwisko</label>
          <input id="inputName" class="form-control" placeholder="Imię i nazwisko" autofocus="" type="text" aria-describedby="helpBlock" disabled>
          <span id="helpBlock" class="help-block">Twoje imię i nazwisko</span>

          <label for="inputAlbum" class="sr-only">Numer albumu</label>
          <input id="inputAlbum" class="form-control" placeholder="Numer albumu" autofocus="" type="text" aria-describedby="helpBlock" disabled>
          <span id="helpBlock" class="help-block">Znajduje się na Twojej legitymacji, ma 6 cyfr.</span>

          <label for="selectDepartment" class="sr-only">Wydział</label>
          <select id="selectDepartment" class="form-control" disabled>
            <option>Wybierz wydział...</option>
            <option>WZIM</option>
            <option>WIP</option>
            <option>WNE</option>
          </select>
          <span id="helpBlock" class="help-block">Wydział do którego jesteś przypisany.</span>

          <label for="inputEmail" class="sr-only">Adres email</label>
          <input id="inputEmail" class="form-control" placeholder="Adres email" autofocus="" type="email" aria-describedby="helpBlock">
          <span id="helpBlock" class="help-block">Adres, który podawałeś przy rejestracji na studia.</span>

          
        </form>
        
      </div>

      <div class="col-md-5">
        <h4>Konto i adres zamieszkania</h4>
        <form>
          <label for="inputNumber" class="sr-only">Numer konta</label>
          <input id="inputNumber" class="form-control" placeholder="Numer konta" autofocus="" type="email" aria-describedby="helpBlock">
          <span id="helpBlock" class="help-block">Twój numer konta</span>

          <label for="inputNumber" class="sr-only">Ulica, numer domu i numer mieszkania</label>
          <input id="inputNumber" class="form-control" placeholder="Ulica, numer domu i numer mieszkania" autofocus="" type="email" aria-describedby="helpBlock">
          <span id="helpBlock" class="help-block">Twój adres</span>

          <label for="inputNumber" class="sr-only">Kod pocztowy</label>
          <input id="inputNumber" class="form-control" placeholder="Kod pocztowy" autofocus="" type="email" aria-describedby="helpBlock">
          <span id="helpBlock" class="help-block">Twoj kod pocztowy</span>

          <label for="inputNumber" class="sr-only">Miejscowość</label>
          <input id="inputNumber" class="form-control" placeholder="Miejscowość" autofocus="" type="email" aria-describedby="helpBlock">
          <span id="helpBlock" class="help-block">Miejscowość w której mieszkasz</span>
        </form>
      </div>
      <div class="col-md-5">
        <form>
          <h4>Zmień swoje hasło</h4>
          <label for="inputPassword" class="sr-only">Stare hasło</label>
          <input id="inputPassword" class="form-control" placeholder="Hasło" type="password" aria-describedby="helpBlock">
          <span id="helpBlock" class="help-block">Weryfikujemy czy jesteś właścicielem tego konta</span>

          <label for="inputPassword" class="sr-only">Hasło</label>
          <input id="inputPassword" class="form-control" placeholder="Hasło" type="password" aria-describedby="helpBlock">
          <span id="helpBlock" class="help-block">Nowe hasło - pamiętaj, aby było bezpieczne!</span>
          <div class="checkbox">
            <label>
              <input value="show-password" type="checkbox"> Pokaż hasło
            </label>
          </div>
          <a href="dashboard.html">
            <button class="btn btn-lg btn-primary btn-block" type="submit" >Zapisz</button>
          </a>
          
        </form>
      </div>
    </div>
  </div> <!-- /container -->

  <!-- /Main -->

  <div class="clearfix"></div>
  <footer class="text-center">This Bootstrap 3 dashboard layout is compliments of <a href="http://www.bootply.com/85850"><strong>Bootply.com</strong></a></footer>


  <div class="modal" id="addWidgetModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Add Widget</h4>
        </div>
        <div class="modal-body">
          <p>Add a widget stuff here..</p>
        </div>
        <div class="modal-footer">
          <a href="#" class="btn">Close</a>
          <a href="#" class="btn btn-primary">Save changes</a>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dalog -->
  </div><!-- /.modal -->




  <!-- script references -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>
</html>