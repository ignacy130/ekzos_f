<!DOCTYPE html>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="http://getbootstrap.com/favicon.ico">

<title>EKZOS - Logowanie</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/signin.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="index_files/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>

    <body>

      <div class="container">
      <div class="row">
          <div class="col-md-offset-2 col-md-8 jumbotron text-center">
            <h1>EKZOS</h1>
            <div class="lead">Tw�j elektroniczny system studenta</div>
            <div class="lead">Rejestracja</div>
            <form action="" modelAttribute="UserModel" method="POST" class="form-signin">
              
              <label for="inputEmail" class="sr-only">Numer albumu</label>
              <input id="inputEmail" path="albumNumber" class="form-control" placeholder="Numer albumu" autofocus="" type="email" aria-describedby="helpBlock"><span id="helpBlock" class="help-block">Znajduje si? na Twojej legitymacji, ma 6 cyfr.</span>
              
              <label for="inputEmail" class="sr-only">Adres email</label>
              <input id="inputEmail" path="email" class="form-control" placeholder="Adres email" autofocus="" type="email" aria-describedby="helpBlock">

              <span id="helpBlock" class="help-block">Adres, kt�ry podawa?e? przy rejestracji na studia.</span>

              <label for="inputPassword" class="sr-only">Has?o</label>
              <input id="inputPassword" path="password" class="form-control" placeholder="Has?o" type="password" aria-describedby="helpBlock">

              <span id="helpBlock" class="help-block">Has?o - pami?taj, aby by?o bezpieczne!</span>
              <div class="checkbox">
                <label>
                  <input value="show-password" type="checkbox"> Poka? has?o
                  <c: out value = "${password}" />
                </label>
              </div>
              
              <label for="selectDepartment" class="sr-only">Wydzia?</label>
                <select id="selectDepartment" class="form-control" path="department">
                  <option>Wybierz wydzia?...</option>
                  <option>WZIM</option>
                  <option>WIP</option>
                  <option>WNE</option>
                </select>

              <span id="helpBlock" class="help-block">Wydzia? do kt�rego jeste? przypisany.</span>

              <br/>
              <a href="dashboard.html">
                  <button class="btn btn-lg btn-primary btn-block" type="submit" value="usun" >
              </a>
              <br/>

              <button class="btn btn-info btn-block">Logowanie</button>
            </form>
          </div>
        </div>
      </div> <!-- /container -->


      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="index_files/ie10-viewport-bug-workaround.js"></script>


    </body></html>