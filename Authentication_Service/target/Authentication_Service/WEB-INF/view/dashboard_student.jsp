<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
  <meta charset="utf-8"/>
  <title>EKZOS - Twoje studenckie konto</title>
  <meta name="generator" content="Bootply" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
  <link href="css/bootstrap.css" rel="stylesheet"/>
  <link href="css/styles.css" rel="stylesheet"/>
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
      <link href="css/styles.css" rel="stylesheet"/>
    </head>
    <body>
      <!-- Header -->
      <div id="top-nav" class="navbar navbar-inverse navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-toggle"></span>
            </button>
            <ul class="nav navbar-nav navbar-left">
              <li >
                <a class="navbar-brand " href="#">EKZOS</a>
              </li>
            </ul>
          </div>
          
          <ul class="nav navbar-nav navbar-right">
            <li class="active">
              <a >h
               Jan Kowalski, #123654
             </a>
           </li>
           <li><a href="settings.htm">Ustawienia</a></li>
           <li><a href="#">Wyloguj</a></li>

         </ul>


       </div><!-- /container -->
     </div>
     <!-- /Header -->

     <!-- Main -->
     <div class="container">

     
   
   <!-- /span-3 -->
   <!-- user menu end -->

<div class="row">
   <div class="col-md-4">
     <!-- saldo -->

     <div >
      <h2 class="text-center">
        Saldo: -500zł
      </h2>

      <button class="btn btn-lg btn-warning btn-block" >Ureguluj</button>
    </div>
    <!-- saldo end -->
    <!-- notifications -->
    <div >
      
      <div class="panel panel-default top-margin" >
        <div class="panel-heading">
          <h4>Powiadomienia</h4>
        </div>
        <div class="panel-body">
          <div class="list-group">

            <a class="list-group-item active" href="#">

              Lorem profile dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 

            </a>
            <a class="list-group-item" href="#">

              Lorem profile dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 

            </a>
            <a class="list-group-item" href="#">

              Lorem profile dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 

            </a>
            <a class="list-group-item" href="#">

              Lorem profile dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 

            </a> 
          </div>
        </div>
      </div>
    </div>
    <!-- notifications end -->
  </div>

  <!-- operations -->
  <div class="col-md-8">
    <h2>Operacje</h2>
    <table class="table table-striped">
      <thead>
        <tr>
          <th class="col-md-2">Data</th>
          <th class="col-md-2">Data zapłaty</th>
          <th class="col-md-2">Kwota</th>
          <th>Tytuł</th>
          <th>Akcja</th>
        </tr>
      </thead>
      <tbody>
        <tr class="danger">
          <td>12:48 12/09/2012</td>
          <td>12:48 10/09/2012</td>
          <td>-1234 zł</td>
          <td>
            Sam sapien massa, aliquam in cursus ut, ullamcorper in tortor. 
            Aliquam mauris arcu, tristique a lobortis vitae, condimentum feugiat justo.
          </td>
          <td>
            <button class="btn">Ureguluj</button>
          </td>
        </tr>
         <tr class="warning">
          <td>12:48 12/09/2012</td>
          <td>12:48 13/09/2012</td>
          <td>-1234 zł</td>
          <td>
            After RWD massa, aliquam in cursus ut, ullamcorper in tortor. 
            Aliquam mauris arcu, tristique a lobortis vitae, condimentum feugiat justo.
          </td>
          <td>
            <button class="btn">Ureguluj</button>
          </td>
        </tr>
        <tr>
          <td>98</td>
          <td>25%</td>
          <td>Type</td>
          <td>
            Wil sapien massa, aliquam in cursus ut, ullamcorper in tortor. 
            Liquam mauris arcu, tristique a lobortis vitae, condimentum feugiat justo.
          </td>
          <td>
            <button class="btn">Ureguluj</button>
          </td>
        </tr>
        <tr>
          <td>109</td>
          <td>8%</td>
          <td>..</td>
          <td>
            Forfoot aliquam in cursus ut, ullamcorper in tortor. 
            Okma mauris arcu, tristique a lobortis vitae, condimentum feugiat justo.
          </td>
          <td>
            <button class="btn disabled">Ureguluj</button>
          </td>
        </tr>
        <tr>
          <td>34</td>
          <td>14%</td>
          <td>..</td>
          <td>
            Mikel sapien massa, aliquam in cursus ut, ullamcorper in tortor. 
            Maliquam mauris arcu, tristique a lobortis vitae, condimentum feugiat justo.
          </td>
          <td>
            <button class="btn">Ureguluj</button>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  </div>
  <!-- operations end -->           
</div>




</div><!--/container-->
<!-- /Main -->


<footer class="text-center">This Bootstrap 3 dashboard layout is compliments of <a href="http://www.bootply.com/85850"><strong>Bootply.com</strong></a></footer>


<div class="modal" id="addWidgetModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Add Widget</h4>
      </div>
      <div class="modal-body">
        <p>Add a widget stuff here..</p>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn">Close</a>
        <a href="#" class="btn btn-primary">Save changes</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dalog -->
</div><!-- /.modal -->




<!-- script references -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>