<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="http://getbootstrap.com/favicon.ico">

<title>EKZOS - Logowanie</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/signin.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="index_files/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>

    <body style="padding:0;">

      <div class="container">
        <div class="row" >
          <div class="col-md-offset-2 col-md-8 jumbotron text-center" style="padding-top: 10px;">
            <h1 class="brand">EKZOS</h1>
            <div class="lead">Twój elektroniczny system studenta</div>
            <div class="lead">Rejestracja</div>
            <form class="form-signin form-horizontal">

              <div class="form-group">
                <label for="inputAlbum" class="col-sm-3 control-label">Numer albumu</label>
                <div class="col-sm-9">
                  <input id="inputAlbum" class="form-control" placeholder="Znajduje się na Twojej legitymacji, ma 6 cyfr." autofocus="" type="email" aria-describedby="helpBlock">
                </div>
              </div>

              <div class="form-group">
                <label for="inputEmail" class="col-sm-3 control-label">Adres email</label>
                <div class="col-sm-9">
                  <input id="inputEmail" class="form-control" placeholder="Adres, który podawałeś przy rejestracji na studia." autofocus="" type="email" aria-describedby="helpBlock">
                </div>
              </div>

              <div class="form-group">
              <label for="inputPassword" class="col-sm-3 control-label">Hasło</label>
                <div class="col-sm-6">
                  <input id="inputPassword" class="form-control" placeholder="Pamiętaj, aby było bezpieczne!" type="password" aria-describedby="helpBlock">
                </div>
                <div class="checkbox col-sm-3">
                    <label>
                      <input value="show-password" type="checkbox"> Pokaż hasło
                    </label>
                  </div>
              </div>
              
              <div class="form-group">
                <label for="selectDepartment" class="col-sm-3 control-label">Wydział</label>
                <div class="col-sm-9">
                  <select id="selectDepartment" class="form-control">
                    <option>Wybierz wydział do którego jesteś przypisany</option>
                    <option>WZIM</option>
                    <option>WIP</option>
                    <option>WNE</option>
                  </select>

                </div>
              </div>
              <br/>
              <a href="dashboard.html">
                <button class="btn btn-lg btn-primary btn-block" type="submit" >Wyślij!</button>
              </a>
              <br/>

              <button class="btn btn-info btn-block">Logowanie</button>
            </form>
          </div>
        </div>
      </div> <!-- /container -->


      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="index_files/ie10-viewport-bug-workaround.js"></script>


    </body></html>