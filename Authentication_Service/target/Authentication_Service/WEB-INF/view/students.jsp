<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
  <meta charset="utf-8"/>
  <title>EKZOS - Twoje studenckie konto</title>
  <meta name="generator" content="Bootply" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
  <link href="css/bootstrap.css" rel="stylesheet"/>
  <link href="css/styles.css" rel="stylesheet"/>
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
      <link href="css/styles.css" rel="stylesheet"/>
    </head>
    <body>
      <!-- Header -->
      
      <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-toggle"></span>
            </button>
            <ul class="nav navbar-nav navbar-left">
              <li class="">
                <a class="navbar-brand " href="#">EKZOS</a>
              </li>
              <li><a href="#">Studenci</a></li>
              <li><a href="#">Operacje</a></li>
            </ul>
          </div>

          <ul class="nav navbar-nav navbar-right">
            <li class="active">
              <a >
               pracownik Jan Kowalski, #123654
             </a>
           </li>
           <li><a href="#">Ustawienia</a></li>
           <li><a href="#">Wyloguj</a></li>

         </ul>


       </div>
     </div>
     <!-- /Header -->

     <!-- Main -->
     <div class="container">
       <!-- students -->
      <div class="row">
      <!-- students -->
        <div class="col-md-12">
            <h2>Studenci</h2>
            <table class="table" style="margin-bottom: 0;">
              <thead>
                <tr>
                  <th class="col-md-2">Kierunek</th>
                  <th class="col-md-2">Numer albumu</th>
                  <th class="col-md-2">Nazwisko, imię</th>
                  <th class="col-md-1">Rok</th>
                  <th class="col-md-2">Saldo</th>
                  <th class="col-md-3">Akcje</th>
                </tr>
              </thead>
            </table>
            <div class="long-list">
              <table class="table table-striped">
                <thead style="opacity: 0; height: 0px; margin: 0; padding: 0;">
                  <tr>
                    <th class="col-md-2"></th>
                    <th class="col-md-2"></th>
                    <th class="col-md-2"></th>
                    <th class="col-md-1"></th>
                    <th class="col-md-2"></th>
                    <th class="col-md-3"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="danger">
                    <td>
                      Informatyka
                    </td>
                    <td>666999</td>
                    <td>Chodkowski, Mateusz</td>
                    
                    <td>
                      2/3
                    </td>
                    <td>-1000 zł</td>
                    <td>
                      <button class="btn">Obciąż / Przyznaj</button>
                      <button class="btn">Więcej</button>
                    </td>
                  </tr>
                  <tr class="danger">
                    <td>
                      Informatyka
                    </td>
                    <td>666999</td>
                    <td>Chodkowski, Mateusz</td>
                    
                    <td>
                      2/3
                    </td>
                    <td>-1000 zł</td>
                    <td>
                      <button class="btn">Obciąż / Przyznaj</button>
                      <button class="btn">Więcej</button>
                    </td>
                  </tr>
                  <tr class="danger">
                    <td>
                      Informatyka
                    </td>
                    <td>666999</td>
                    <td>Chodkowski, Mateusz</td>
                    
                    <td>
                      2/3
                    </td>
                    <td>-1000 zł</td>
                    <td>
                      <button class="btn">Obciąż / Przyznaj</button>
                      <button class="btn">Więcej</button>
                    </td>
                  </tr>
                  <tr class="danger">
                    <td>
                      Informatyka
                    </td>
                    <td>666999</td>
                    <td>Chodkowski, Mateusz</td>
                    
                    <td>
                      2/3
                    </td>
                    <td>-1000 zł</td>
                    <td>
                      <button class="btn">Obciąż / Przyznaj</button>
                      <button class="btn">Więcej</button>
                    </td>
                  </tr>
                  <tr class="danger">
                    <td>
                      Informatyka
                    </td>
                    <td>666999</td>
                    <td>Chodkowski, Mateusz</td>
                    
                    <td>
                      2/3
                    </td>
                    <td>-1000 zł</td>
                    <td>
                      <button class="btn">Obciąż / Przyznaj</button>
                      <button class="btn">Więcej</button>
                    </td>
                  </tr>
                  <tr class="danger">
                    <td>
                      Informatyka
                    </td>
                    <td>666999</td>
                    <td>Chodkowski, Mateusz</td>
                    
                    <td>
                      2/3
                    </td>
                    <td>-1000 zł</td>
                    <td>
                      <button class="btn">Obciąż / Przyznaj</button>
                      <button class="btn">Więcej</button>
                    </td>
                  </tr>
                  <tr class="danger">
                    <td>
                      Informatyka
                    </td>
                    <td>666999</td>
                    <td>Chodkowski, Mateusz</td>
                    
                    <td>
                      2/3
                    </td>
                    <td>-1000 zł</td>
                    <td>
                      <button class="btn">Obciąż / Przyznaj</button>
                      <button class="btn">Więcej</button>
                    </td>
                  </tr>
                  <tr class="danger">
                    <td>
                      Informatyka
                    </td>
                    <td>666999</td>
                    <td>Chodkowski, Mateusz</td>
                    
                    <td>
                      2/3
                    </td>
                    <td>-1000 zł</td>
                    <td>
                      <button class="btn">Obciąż / Przyznaj</button>
                      <button class="btn">Więcej</button>
                    </td>
                  </tr>
                  <tr class="danger">
                    <td>
                      Informatyka
                    </td>
                    <td>666999</td>
                    <td>Chodkowski, Mateusz</td>
                    
                    <td>
                      2/3
                    </td>
                    <td>-1000 zł</td>
                    <td>
                      <button class="btn">Obciąż / Przyznaj</button>
                      <button class="btn">Więcej</button>
                    </td>
                  </tr>
                  <tr class="danger">
                    <td>
                      Informatyka
                    </td>
                    <td>666999</td>
                    <td>Chodkowski, Mateusz</td>
                    
                    <td>
                      2/3
                    </td>
                    <td>-1000 zł</td>
                    <td>
                      <button class="btn">Obciąż / Przyznaj</button>
                      <button class="btn">Więcej</button>
                    </td>
                  </tr>
                  <tr class="warning">
                    <td>Chodkowski, Mateusz</td>
                    <td>666999</td>
                    <td>
                      Informatyka
                    </td>
                    <td>
                      2/3
                    </td>
                    <td>-1000 zł</td>
                    <td>
                      <button class="btn">Obciąż / Przyznaj</button>
                      <button class="btn">Więcej</button>
                    </td>
                  </tr>
                  <tr>
                    <td>Chodkowski, Mateusz</td>
                    <td>666999</td>
                    <td>
                      Informatyka
                    </td>
                    <td>
                      2/3
                    </td>
                    <td>-1000 zł</td>
                    <td>
                      <button class="btn">Obciąż / Przyznaj</button>
                      <button class="btn">Więcej</button>
                    </td>
                  </tr>
                  <tr>
                    <td>Chodkowski, Mateusz</td>
                    <td>666999</td>
                    <td>
                      Informatyka
                    </td>
                    <td>
                      2/3
                    </td>
                    <td>-1000 zł</td>
                    <td>
                      <button class="btn">Obciąż / Przyznaj</button>
                      <button class="btn">Więcej</button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
        </div>
      <!-- students end -->
      </div>

  </div>


</div><!--/container-->
<!-- /Main -->


<footer class="text-center">This Bootstrap 3 dashboard layout is compliments of <a href="http://www.bootply.com/85850"><strong>Bootply.com</strong></a></footer>


<div class="modal" id="addWidgetModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Add Widget</h4>
      </div>
      <div class="modal-body">
        <p>Add a widget stuff here..</p>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn">Close</a>
        <a href="#" class="btn btn-primary">Save changes</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dalog -->
</div><!-- /.modal -->




<!-- script references -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>