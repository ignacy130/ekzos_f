<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
  <meta charset="utf-8"/>
  <title>EKZOS - Wyślij</title>
  <meta name="generator" content="Bootply" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
  <link href="css/bootstrap.css" rel="stylesheet"/>
  <link href="css/styles.css" rel="stylesheet"/>
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
      <link href="css/styles.css" rel="stylesheet"/>
    </head>
    <body>
      <!-- Header -->
      <div id="top-nav" class="navbar navbar-inverse navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-toggle"></span>
            </button>
            <ul class="nav navbar-nav navbar-left">
              <li class="active">
                <a class="navbar-brand " href="#">EKZOS - Twoje studenckie konto</a>
              </li>
            </ul>
          </div>
          
          <ul class="nav navbar-nav navbar-right">
            <li class="active">
              <a>
               Jan Kowalski, #123654
             </a>
           </li>
           <li><a href="settings.htm">Ustawienia</a></li>
           <li><a href="#">Wyloguj</a></li>

         </ul>


       </div><!-- /container -->
     </div>
     <!-- /Header -->

     <!-- Main -->

   </div>
   <!-- /span-3 -->
   <!-- user menu end -->

   <div class="container">


     <div class="col-md-6">
      <!-- operations-->
      <div class="col-md-12">
        <h2>Studenci</h2>
        <table class="table" style="margin-bottom: 0;">
          <thead>
            <tr>
              <th class="col-md-3">Kierunek</th>
              <th class="col-md-2">Album</th>
              <th class="col-md-3">Nazwisko, imię</th>
              <th class="col-md-1">Rok</th>
              <th class="col-md-2">Saldo</th>
              <th class="col-md-1">Zaznacz</th>
            </tr>
          </thead>
        </table>
        <div class="long-list">
          <table class="table table-striped">
            <thead style="opacity: 0; height: 0px; margin: 0; padding: 0;">
              <tr>
                <th class="col-md-3"></th>
                <th class="col-md-2"></th>
                <th class="col-md-3"></th>
                <th class="col-md-1"></th>
                <th class="col-md-2"></th>
                <th class="col-md-1"></th>
              </tr>
            </thead>
            <tbody>
              <tr class="danger">
                <td>
                  Informatyka
                </td>
                <td>666999</td>
                <td>Chodkowski, Mateusz</td>

                <td>
                  2/3
                </td>
                <td>-1000 zł</td>
                <td>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div>
                </td>
              </tr>
              <tr class="danger">
                <td>
                  Informatyka
                </td>
                <td>666999</td>
                <td>Chodkowski, Mateusz</td>

                <td>
                  2/3
                </td>
                <td>-1000 zł</td>
                <td>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div>
                </td>
              </tr>
              <tr class="danger">
                <td>
                  Informatyka
                </td>
                <td>666999</td>
                <td>Chodkowski, Mateusz</td>

                <td>
                  2/3
                </td>
                <td>-1000 zł</td>
                <td>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div>
                </td>
              </tr>
              <tr class="danger">
                <td>
                  Informatyka
                </td>
                <td>666999</td>
                <td>Chodkowski, Mateusz</td>

                <td>
                  2/3
                </td>
                <td>-1000 zł</td>
                <td>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div>
                </td>
              </tr>
              <tr class="danger">
                <td>
                  Informatyka
                </td>
                <td>666999</td>
                <td>Chodkowski, Mateusz</td>

                <td>
                  2/3
                </td>
                <td>-1000 zł</td>
                <td>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div>
                </td>
              </tr>
              <tr class="danger">
                <td>
                  Informatyka
                </td>
                <td>666999</td>
                <td>Chodkowski, Mateusz</td>

                <td>
                  2/3
                </td>
                <td>-1000 zł</td>
                <td>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div>
                </td>
              </tr>
              <tr class="danger">
                <td>
                  Informatyka
                </td>
                <td>666999</td>
                <td>Chodkowski, Mateusz</td>

                <td>
                  2/3
                </td>
                <td>-1000 zł</td>
                <td>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div>
                </td>
              </tr>
              <tr class="danger">
                <td>
                  Informatyka
                </td>
                <td>666999</td>
                <td>Chodkowski, Mateusz</td>

                <td>
                  2/3
                </td>
                <td>-1000 zł</td>
                <td>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div>
                </td>
              </tr>
              <tr class="danger">
                <td>
                  Informatyka
                </td>
                <td>666999</td>
                <td>Chodkowski, Mateusz</td>

                <td>
                  2/3
                </td>
                <td>-1000 zł</td>
                <td>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div>
                </td>
              </tr>
              <tr class="danger">
                <td>
                  Informatyka
                </td>
                <td>666999</td>
                <td>Chodkowski, Mateusz</td>

                <td>
                  2/3
                </td>
                <td>-1000 zł</td>
                <td>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div>
                </td>
              </tr>
              <tr class="warning">
                <td>Chodkowski, Mateusz</td>
                <td>666999</td>
                <td>
                  Informatyka
                </td>
                <td>
                  2/3
                </td>
                <td>-1000 zł</td>
                <td>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div>
                </td>
              </tr>
              <tr>
                <td>Chodkowski, Mateusz</td>
                <td>666999</td>
                <td>
                  Informatyka
                </td>
                <td>
                  2/3
                </td>
                <td>-1000 zł</td>
                <td>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div>
                </td>
              </tr>
              <tr>
                <td>Chodkowski, Mateusz</td>
                <td>666999</td>
                <td>
                  Informatyka
                </td>
                <td>
                  2/3
                </td>
                <td>-1000 zł</td>
                <td>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <!-- operations end -->
    </div>


    <div class="col-md-6">
      <h2>Transfer</h2>
      <form class="form">

        <div class="radio">
          <label style="border-right: 1px solid #ddd; padding-right: 10px; margin-right: 5px;">
            <input type="radio" name="typeRadios" id="optionsRadios1" value="option1"  checked>
            Uznanie
          </label>
          <label>
            <input type="radio" name="typeRadios" id="optionsRadios1" value="option1">
            Obciążenie
          </label>
        </div>

        <label for="inputName" class="sr-only">Numer rachunku nadawcy</label>
        <input id="inputName" class="form-control" placeholder="Numer rachunku nadawcy" autofocus="" type="text" aria-describedby="helpBlock" disabled>
        <span id="helpBlock" class="help-block">Twój numer rachunku. Możesz go zmienić w <a href="#"> ustawieniach.</a></span>

        <label for="inputName" class="sr-only">Nazwa / godność</label>
        <input id="inputName" class="form-control" placeholder="Nazwa / godność" autofocus="" type="text" aria-describedby="helpBlock">
        <span id="helpBlock" class="help-block">Nazwa instytucji / imię i nazwisko odbiorcy</span>

        <label for="inputAmount" class="sr-only">Kwota</label>
        <input id="inputAmount" class="form-control" placeholder="Kwota" autofocus="" type="text" aria-describedby="helpBlock">
        <span id="helpBlock" class="help-block">Kwota transakcji</span>

        <label for="inputAddress" class="sr-only">Adres</label>
        <input id="inputAddress" class="form-control" placeholder="Adres fizyczny" autofocus="" type="text" aria-describedby="helpBlock">
        <span id="helpBlock" class="help-block">Adres korespondencyjny odbiorcy.</span>

        <label for="inputNumber" class="sr-only">Numer rachunku odbiorcy</label>
        <input id="inputNumber" class="form-control" placeholder="Numer rachunku odbiorcy" type="text" aria-describedby="helpBlock" required>
        <span id="helpBlock" class="help-block">Numer rachunku na który chcesz wysłać pieniądze</span>

        <label for="inpuTitle" class="sr-only">Tytuł przelewu</label>
        <input id="inputTitle" class="form-control" placeholder="Tytuł przelewu" type="text" aria-describedby="helpBlock">
        <span id="helpBlock" class="help-block">Tytuł Twojego przelewu, zalecamy podanie numeru operacji/faktury</span>

        <br/>
        <a href="dashboard.html">
          <button class="btn btn-lg btn-primary btn-block disabled" type="submit" >Zrealizuj przelew</button>
        </a>
        <br/>
      </form>
    </div>


  </div>

</div><!--/container-->
<!-- /Main -->

<div class="clearfix"></div>
<footer class="text-center">This Bootstrap 3 dashboard layout is compliments of <a href="http://www.bootply.com/85850"><strong>Bootply.com</strong></a></footer>


<div class="modal" id="addWidgetModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Add Widget</h4>
      </div>
      <div class="modal-body">
        <p>Add a widget stuff here..</p>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn">Close</a>
        <a href="#" class="btn btn-primary">Save changes</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dalog -->
</div><!-- /.modal -->




<!-- script references -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>