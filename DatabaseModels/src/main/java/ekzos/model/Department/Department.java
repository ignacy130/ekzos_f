package ekzos.model.Department;

import ekzos.model.EntityBase.EntityBase;
import ekzos.model.User.UserModel;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

//import org.hibernate.annotations.Table;


/**
 * Created by Paweł on 2015-01-03.
 */

@Entity
@Table(name="Department")
public class Department extends EntityBase implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Id")
    private int Id;

    public Department() {
    }
    public int getId() {
        return Id;
    }
    public void setId(int value){
        this.Id = value;
    }
    
    @Column(name = "name")
    private String name = "";

    @ManyToMany
    @JoinTable(name="Department_userList",
                joinColumns = {@JoinColumn(name="Department_Id")},
                inverseJoinColumns = {@JoinColumn(name="UserModel_Id")})
    private Set<UserModel> userList;

    public Department(String name, Set<UserModel> userList)
    {
        this.name = name;
        this.userList = userList;
    }
    
    //region Accessors
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Set<UserModel> getUserList()
    {
        return userList;
    }

    public void setUserList(Set<UserModel> userList)
    {
        this.userList = userList;
    }
    //endregion


}
