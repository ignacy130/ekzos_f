package ekzos.model.Operation;

import ekzos.model.User.AccountBank;
import java.io.Serializable;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Paweł on 2015-01-03.
 */
@Entity
@Table(name="FinancialOperation")
public class FinancialOperation  implements Serializable {
      
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Id")
    private int Id;

    public FinancialOperation() {
    }
    
    public int getId() {
        return Id;
    }
    public void setId(int value){
        this.Id = value;
    }
    
    @Column(name="date")
    Date date;

    @Column(name="amount")
    BigDecimal amount;

    @Column(name="title")
    String title;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id")
    AccountBank account;

    public FinancialOperation(Date date, BigDecimal amount, String title, AccountBank account) {
        this.date = date;
        this.amount = amount;
        this.title = title;
        this.account = account;
    }

    //region Getters
    public AccountBank getAccount() {
        return account;
    }

    public Date getDate() {
        return date;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getTitle() {
        return title;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAccount(AccountBank account) {
        this.account = account;
    }
//endregion
}
