package ekzos.model.Operation;

import ekzos.model.User.AccountBank;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Paweł on 2015-01-03.
 */
@MappedSuperclass
public class OperationBase {

    public OperationBase(){};
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Id")
    private int Id;
    public int getId() {
        return Id;
    }
    public void setId(int value){
        this.Id = value;
    }
    
    @Column(name="date")
    Date date;

    @Column(name="amount")
     BigDecimal amount;

    @Column(name="title")
     String title;

    @ManyToOne(optional = false)
    @JoinColumn(name = "AccountType_Id",nullable = false )
     AccountBank account;

    public OperationBase(Date date, BigDecimal amount, String title, AccountBank account) {
        this.date = date;
        this.amount = amount;
        this.title = title;
        this.account = account;
    }

    //region Getters
    public AccountBank getAccount() {
        return account;
    }

    public Date getDate() {
        return date;
    }

    protected void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getTitle() {
        return title;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAccount(AccountBank account) {
        this.account = account;
    }

//endregion
}
