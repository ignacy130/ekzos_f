package ekzos.model.Operation;

/**
 * Created by Paweł on 2015-01-03.
 */
public enum PeriodType {

    Day(1),
    Week(7*Day.length),
    TwoWeek(2*Week.length),
    Month(4*Week.length),
    Quarter(3*Month.length),
    HalfYear(6*Month.length),
    Year(12*Month.length);

    private int length;
    private PeriodType(int length){
        this.length =length;
    }

    public int getLength(){
        return this.length;
    }

}
