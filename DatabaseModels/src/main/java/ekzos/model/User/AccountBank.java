package ekzos.model.User;

import ekzos.model.EntityBase.EntityBase;
import ekzos.model.Operation.FinancialOperation;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * Created by Paweł on 2015-01-03.
 */
@Entity
@Table(name="AccountBank")
public class AccountBank implements Serializable {
    
    @Column(name = "balance")
    private BigDecimal balance;
    @OneToMany(mappedBy = "account")
    private List<FinancialOperation> financialOperations;
    @Column(name = "number")
    private String number;
    @OneToOne
    private UserModel user;
    @Column(name = "bankName")
    private String bankName;
    
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Id")
    @Id
    private int Id;
    public int getId() {
        return Id;
    }
    public void setId(int value){
        this.Id = value;
    }
    
    public String getBankName()
    {
        return bankName;
    }

    public void setBankName(String bankName)
    {
        this.bankName = bankName;
    }

    public AccountBank(){
        this.financialOperations = new ArrayList<FinancialOperation>();
    }
    
    public AccountBank(BigDecimal balance, List<FinancialOperation> financialOperations, String number, UserModel user)
    {
        this.balance = balance;
        this.financialOperations = financialOperations;
        this.number = number;
        this.user = user;
    }

    //region Accessors
    public BigDecimal getBalance()
    {
        return balance;
    }

    public void setBalance(BigDecimal balance)
    {
        this.balance = balance;
    }

    public List<FinancialOperation> getFinancialOperations()
    {
        return financialOperations;
    }

    public void setFinancialOperations(List<FinancialOperation> financialOperations)
    {
        this.financialOperations = financialOperations;
    }

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    public UserModel getUser()
    {
        return user;
    }

    public void setUser(UserModel user)
    {
        this.user = user;
    }

}
