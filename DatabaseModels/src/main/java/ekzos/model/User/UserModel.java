package ekzos.model.User;

import ekzos.model.Department.Department;
import ekzos.model.EntityBase.EntityBase;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Paweł on 2015-01-03.
 */

@Entity
@Table(name = "UserModel")
public class UserModel extends EntityBase implements Serializable {

    public UserModel() {
        this.departments = new HashSet<Department>();
        this.street = "";
        this.city = "";
        this.flatNumber=1;
        this.houseNumer=1;
        this.name="";
        this.nip="";
        this.surName="";
        this.zip="";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Id")
    private int id;

    private String albumNumber;

    private String login;
    @Column(name = "password", nullable = false, length = 60)
    private String password;
    @Column(name = "username", unique = true, nullable = false, length = 45)
    public String email;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "type_id")
    private UserType type;

    @OneToOne(cascade = CascadeType.ALL)
    private AccountBank account;

    private String name;

    private String surName;

    private String street;

    private int houseNumer;

    private int flatNumber;

    private String city;

    private String zip;

    private String nip;

    private boolean enabled;

    @ManyToMany(mappedBy = "userList")
    private Set<Department> departments;


    public Set<Department> getDepartments() {
        return departments;
    }


    public void setDepartments(Set<Department> value) {
        departments = value;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }


    /**
     * For register
     */
    public UserModel(String albumNumber, String login, String password, String email, UserType type, AccountBank account, String name, String surName, String street, int houseNumer, int flatNumber, String city, String zip, String nip, boolean enabled) {
        this.albumNumber = albumNumber;
        this.login = login;
        this.password = password;
        this.email = email;
        this.type = type;
        this.account = account;
        this.name = name;
        this.surName = surName;
        this.street = street;
        this.houseNumer = houseNumer;
        this.flatNumber = flatNumber;
        this.city = city;
        this.zip = zip;
        this.nip = nip;
        this.enabled = enabled;
    }


    /**
     * for login
     */
    public UserModel(String email, String password, boolean enabled) {
        this.email = email;
        this.password = password;
        this.enabled = enabled;
    }

    public UserModel(String email, String username, String password, boolean enabled) {
        this.email = username;
        this.password = password;
        this.enabled = enabled;
    }


    //region Accessors
    /*
    public void setFullName(String value){
        fullName = value;
    }
    */

    public void setId(int value) {
        this.id = value;
    }


    public int getId() {
        return id;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHouseNumer() {
        return houseNumer;
    }

    public void setHouseNumer(int houseNumer) {
        this.houseNumer = houseNumer;
    }

    /**
     * @return -1 if doesnt exist ex. while having house
     */
    public int getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(int flatNumber) {
        this.flatNumber = flatNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return returns name concateded with surName
     */
    public String getFullName() {
        return this.getName() + " " + this.getSurName();
    }

    public void setFullName(String value) {

    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getAlbumNumber() {
        return albumNumber;
    }

    public void setAlbumNumber(String albumNumber) {
        this.albumNumber = albumNumber;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }


    public AccountBank getAccount() {
        return account;
    }

    public void setAccount(AccountBank account) {
        this.account = account;
    }
    //endregion
}

