package ekzos.model.User;

import ekzos.model.EntityBase.EntityBase;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;


/*
public enum UserType
{
    Student,
    Clerk
} */
@Entity
@Table(name = "UserType")
public class UserType extends EntityBase implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;


    @OneToMany(mappedBy = "type")
    private Set<UserModel> users;

    @Column(name = "role", nullable = false, length = 45)
    private String role;

    public UserType() {
    }

    public UserType(String role) {
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int value) {
        this.id = value;
    }
    /*
    @Column(name = "userRoleId")
    public Integer getUserRoleId() {
        return userRoleId;
    }*/
    /*
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "user_role_id",
            unique = true, nullable = false)
    public void setUserRoleId(Integer userRoleId) {
        this.userRoleId = userRoleId;
    }*/
    /*
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userRole", nullable = true)
    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }*/

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Set<UserModel> getUsers() {
        return users;
    }

    public void setUsers(Set<UserModel> users) {
        this.users = users;
    }
}

