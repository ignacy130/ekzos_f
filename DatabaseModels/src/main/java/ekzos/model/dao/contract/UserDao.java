package ekzos.model.dao.contract;

/**
 * Created by Paweł on 2015-01-07.
 */
import ekzos.model.User.UserModel;

public interface UserDao {

    UserModel findByUserName(String username);

}