package ekzos.model.dao.implementation;

import ekzos.model.User.AccountBank;
import org.springframework.stereotype.Repository;

/**
 * Created by Paweł on 2015-01-03.
 */
@Repository
public class AccountDao extends BaseDao<AccountBank> {

    @Override
    public Class<?> type() {
        return AccountBank.class;
    }

}
