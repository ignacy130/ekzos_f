package ekzos.model.dao.implementation;

import ekzos.model.Department.Department;
import ekzos.model.Operation.FinancialOperation;
import ekzos.model.User.AccountBank;
import ekzos.model.User.UserModel;
import ekzos.model.User.UserType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.hibernate.Transaction;

/**
 * Created by Mateusz on 2015-01-06.
 */
public abstract class BaseDao<T> {
    //konfiguracja: Authentication_service: applicationContext.xml, datasource, seesionFactory = skalowalne bezpieczne rozwoazanie dla aplikacji korporacyjnych JEE
    @Autowired
    private SessionFactory sessionFactory;
    private Transaction currentTransaction;
    
    public SessionFactory getSessionFactory(){
        if(sessionFactory == null)
        {
        AnnotationConfiguration configuration = new AnnotationConfiguration();
        configuration.configure("hibernate.cfg.xml");
        configuration.addAnnotatedClass(Department.class);
        configuration.addAnnotatedClass(FinancialOperation.class);
        configuration.addAnnotatedClass(AccountBank.class);
        configuration.addAnnotatedClass(UserModel.class);
        configuration.addAnnotatedClass(UserType.class);
        ServiceRegistry builder = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).buildServiceRegistry();
        
        sessionFactory = configuration.buildSessionFactory(builder);
        return sessionFactory;
        } else {
            return sessionFactory;
        }
    }
    
    public Session openCurrentSession() {
        Session s = getSessionFactory().getCurrentSession();
        if(s!=null){
            return s ;
        }else {
            getSessionFactory().openSession();
            s = getSessionFactory().getCurrentSession();
            return s;
        }
    }

    public void closeSessionAndTransaction(){
        //sessionFactory.getCurrentSession().getTransaction().commit();
        sessionFactory.getCurrentSession().close();
    }
    
    @Transactional
    public T merge(T entity) { //merge - insert/update
        return (T) openCurrentSession().merge(entity);
    }

    @Transactional
    public void delete(T entity) {
        openCurrentSession().delete(entity);
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public List<T> findAll() {
       return openCurrentSession().createCriteria(type()).list();
    }

    @Transactional
    public T findById(Serializable id){
        return (T) openCurrentSession().get(type(), id);
    }

    public abstract Class<?> type();
}
