package ekzos.model.dao.implementation;
;
import ekzos.model.Department.Department;
import org.springframework.stereotype.Repository;

/**
 * Created by Paweł on 2015-01-03.
 */
@Repository
public class DepartmentDao extends BaseDao<Department> {

    public DepartmentDao(){
        super();
    }
    
    
    @Override
    public Class<?> type() {
        return Department.class;
    }
}
