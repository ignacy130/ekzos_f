package ekzos.model.dao.implementation;

import ekzos.model.Operation.FinancialOperation;
import org.springframework.stereotype.Repository;

/**
 * Created by Paweł on 2015-01-03.
 */
@Repository
public class FinancialDao extends BaseDao<FinancialOperation>{

    @Override
    public Class<?> type() {
        return FinancialOperation.class;
    }
}
