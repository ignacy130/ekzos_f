/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ekzos.model.dao.implementation;

import ekzos.model.User.UserType;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Mateusz
 */
@Repository
public class UserTypeDao extends BaseDao<UserType>  {

    @Override
    public Class<?> type() {
        return UserType.class;
    }
}
