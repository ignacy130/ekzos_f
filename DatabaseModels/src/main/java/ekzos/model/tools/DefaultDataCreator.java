package ekzos.model.tools;

import ekzos.model.Department.Department;
import ekzos.model.Operation.FinancialOperation;
import ekzos.model.User.AccountBank;
import ekzos.model.User.UserModel;
import ekzos.model.User.UserType;
import ekzos.model.dao.contract.UserDao;
import ekzos.model.dao.implementation.AccountDao;
import ekzos.model.dao.implementation.DepartmentDao;
import ekzos.model.dao.implementation.FinancialDao;
import ekzos.model.dao.implementation.UserDaoImpl;
import ekzos.model.dao.implementation.UserTypeDao;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Transactional;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Mateusz
 */
public class DefaultDataCreator {

    public DefaultDataCreator() {
    }

    @Transactional
    public static void main(String[] args) {
        Department dep1 = new Department("WZIM", new HashSet<UserModel>());
        Department dep2 = new Department("WOBIAK", new HashSet<UserModel>());
        Department dep3 = new Department("WNE", new HashSet<UserModel>());
        DepartmentDao dd = new DepartmentDao();
        try
        {
            Session s = dd.openCurrentSession();
            Transaction t = s.beginTransaction();
            dep1 = dd.merge(dep1);
            dep2 = dd.merge(dep2);
            dep3 = dd.merge(dep3);
            t.commit();
        }catch(Exception e)
        {
            dd.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally{
            dd.closeSessionAndTransaction();
        }
        
        AccountBank ab1 = new AccountBank(new BigDecimal(5000),new ArrayList<FinancialOperation>(),"123456789",null);
        AccountBank ab2 = new AccountBank(new BigDecimal(1000),new ArrayList<FinancialOperation>(),"999999999",null);
        ab1.setBankName("PKO");
        ab2.setBankName("PKO");

        AccountDao ad = new AccountDao();
        try
        {
            Session s = ad.openCurrentSession();
            Transaction t = s.beginTransaction();
            ab1 = ad.merge(ab1);
            ab2 = ad.merge(ab2);
            t.commit();
        }catch(Exception e)
        {
            ad.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally{
            ad.closeSessionAndTransaction();
        }
        
        UserType ut1 = new UserType("Admin");
        UserType ut2 = new UserType("Student");
        
        UserTypeDao utd = new UserTypeDao();
        try
        {
            Session s = utd.openCurrentSession();
            Transaction t = s.beginTransaction();
            ut1 = utd.merge(ut1);
            ut2 = utd.merge(ut2);
            t.commit();
        }catch(Exception e)
        {
            utd.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally{
            utd.closeSessionAndTransaction();
        }
        
        UserModel um1 = new UserModel("161111","janek","janek","janek@sggw.pl",ut2,ab2,"Jan","Kowalski","Nowoursynowska",15,1,"Warszawa","02-800","12345679",true);
        UserModel um2 = new UserModel("000000","Admin","Admin123","admin@sggw.pl",ut1,ab1,"AdminWZIM","Admin","Nowoursynowska",159,1,"Warszawa","02-800","000000",true);

        UserDaoImpl umd = new UserDaoImpl();
        try
        {
            Session s = umd.openCurrentSession();
            Transaction t = s.beginTransaction();
            um1 = umd.merge(um1);
            um2 = umd.merge(um2);
            t.commit();
        }catch(Exception e)
        {
            umd.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally{
            umd.closeSessionAndTransaction();
        }
        
        //Departament actualization after adding users
        Set usersOfWzim = new HashSet(){};
        usersOfWzim.add(um1);
        usersOfWzim.add(um2);
        dep1.setUserList(usersOfWzim);
        try
        {
            Session s = dd.openCurrentSession();
            Transaction t = s.beginTransaction();
            dep1 = dd.merge(dep1);
            t.commit();
        }catch(Exception e)
        {
            dd.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally{
            dd.closeSessionAndTransaction();
        }
        
        //AccountBank actualization after adding users
        ab1.setUser(um2);
        ab2.setUser(um1);
        try
        {
            Session s = ad.openCurrentSession();
            Transaction t = s.beginTransaction();
            ab1 = ad.merge(ab1);
            ab2 = ad.merge(ab2);
            t.commit();
        }catch(Exception e)
        {
            ad.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally{
            ad.closeSessionAndTransaction();
        }
        
        //Adding new financial operation
        FinancialOperation fo1 = new FinancialOperation(new Date(),new BigDecimal(350),"Stypendium",ab2);
        FinancialDao fd = new FinancialDao();
        try
        {
            Session s = fd.openCurrentSession();
            Transaction t = s.beginTransaction();
            fo1 = fd.merge(fo1);
            t.commit();
        }catch(Exception e)
        {
            fd.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally{
            fd.closeSessionAndTransaction();
        }
    }

}