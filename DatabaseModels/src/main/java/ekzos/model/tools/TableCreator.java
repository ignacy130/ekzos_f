package ekzos.model.tools;


import ekzos.model.Department.Department;
import ekzos.model.Operation.FinancialOperation;
import ekzos.model.User.AccountBank;
import ekzos.model.User.UserModel;
import ekzos.model.User.UserType;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.tool.hbm2ddl.SchemaExport;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mateusz
 */
public class TableCreator {
    public static void main(String[] args) {

       AnnotationConfiguration ac = new AnnotationConfiguration();
       ac.configure("hibernate.cfg.xml");
       ac.addAnnotatedClass(Department.class);
       ac.addAnnotatedClass(FinancialOperation.class);
       //ac.addAnnotatedClass(ObligationOperation.class);
       ac.addAnnotatedClass(AccountBank.class);
       ac.addAnnotatedClass(UserModel.class);
       ac.addAnnotatedClass(UserType.class);

       new SchemaExport(ac).create(true,true);

    }
}
