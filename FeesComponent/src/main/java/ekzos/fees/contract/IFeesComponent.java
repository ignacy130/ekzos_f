package ekzos.fees.contract; /**
 * Created by ignac_000 on 9/12/2014.
 */

import ekzos.model.Operation.FinancialOperation;
import ekzos.model.Operation.PeriodType;
import ekzos.model.User.AccountBank;
import ekzos.model.User.UserModel;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Contract for FeesComponent.
 */
public interface IFeesComponent
{
    /**
     * Returns list of financial operations for user.
     * @param user - user whose operations will be returned
     * @return list of financial operations of user requested
     */
    List<FinancialOperation> getOperations(UserModel user);

    /**
     * Returns list containing realization dates of financial operations for user.
     * @param user - user whose dates of operations will be returned
     * @return list containing realization dates of financial operations for user.
     */
    List<Date> getMaturities(UserModel user);

    /**
     * Returns list containing values of financial operations for user.
     * @param user - user whose values of operations will be returned
     * @return list containing values of financial operations for user
     */
    List<BigDecimal> getFinancesValues(UserModel user);

    /**
     * Returns list containing interest values of financial operations for user.
     * @param user - user whose interest values of operations will be returned
     * @return list containing interest values of financial operations for user
     */
    List<BigDecimal> getInterestValues(UserModel user);

    /**
     * Sends specified amount of money to specified account.
     * @param account - target of operation
     * @param amount - amount of money
     */
    void sendMoney(AccountBank account, BigDecimal amount, UserModel user, String title);

    /**
     * Sets debt at specified students account.
     * @param user - user who will have debt
     * @param amount - value of debt
     * @param maturity - due date
     * @param interestRate - in percents
     * @param periodType - period of interests increase
     * @param title - title of operation
     */
    void setDue(UserModel user, BigDecimal amount, BigDecimal interestRate, PeriodType periodType, String title, Date maturity);

    /**
     * Cancels specified operation.
     * @param operation - operation to be canceled
     */
    void cancelDue(FinancialOperation operation);

}
