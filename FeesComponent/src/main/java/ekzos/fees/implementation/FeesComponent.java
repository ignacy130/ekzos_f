package ekzos.fees.implementation;

import ekzos.fees.contract.IFeesComponent;
import ekzos.model.Operation.FinancialOperation;
import ekzos.model.Operation.PeriodType;
import ekzos.model.User.AccountBank;
import ekzos.model.User.UserModel;
import ekzos.model.dao.implementation.AccountDao;
import ekzos.model.dao.implementation.FinancialDao;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Created by ignac_000 on 10/12/2014.
 */
public class FeesComponent {

    public static FeesComponent getFeesComponent() {
        return new FeesComponent();
    }

    public List<FinancialOperation> getOperations(UserModel user) {
        return user.getAccount().getFinancialOperations();
    }

    public List<Date> getMaturities(UserModel user) {
        List<Date> dates = new ArrayList<Date>();
        List<FinancialOperation> ops = this.getOperations(user);
        for (FinancialOperation op : ops) {
            dates.add(op.getDate());
        }
        return dates;
    }

    public List<BigDecimal> getFinancesValues(UserModel user) {
        List<BigDecimal> values = new ArrayList<BigDecimal>();
        List<FinancialOperation> ops = this.getOperations(user);
        for (FinancialOperation op : ops) {
            values.add(op.getAmount());
        }
        return values;
    }

    public List<BigDecimal> getInterestValues(UserModel user) {

        List<BigDecimal> values = new ArrayList<>();
        /*
         List<FinancialOperation> ops = this.getOperations(user);
         ops.removeIf(x -> !(x instanceof FinancialOperation));
         for (FinancialOperation op : ops)
         {
         values.add(((FinancialOperation)op).getInterestValue());
         }*/
        return values;
    }
/*
    public boolean sendMoney(BigDecimal amount, AccountBank account, String title) {
        boolean success = true;
        
        Date date = new Date();
        //create operation
        FinancialOperation operation = new FinancialOperation();
        operation.setDate(date);
        operation.setTitle(title);
        operation.setAmount(amount);
        operation.setAccount(account);

        //insert operation
        FinancialDao fd = new FinancialDao();
        Transaction t = null;
        boolean operationInsterted = false;
        try {
            Session s = fd.openCurrentSession();
            t = s.beginTransaction();
            operation = fd.merge(operation);
            
            operationInsterted = true;
        } catch (Exception e) {
            success = false;
            if (t != null) {
                t.rollback();
            }
        } finally {
            if (t != null) {
                t.commit();
            }
            fd.closeSessionAndTransaction();
        }

        //update account
        if (operationInsterted) {
            BigDecimal actualFinanceStatus = account.getBalance().add(amount);
            account.setBalance(actualFinanceStatus);
            AccountDao ad = new AccountDao();
            t = null;
            try {
                Session s = ad.openCurrentSession();
                t = s.beginTransaction();
                account = ad.merge(account);
                
            } catch (Exception e) {
                success = false;
                if (t != null) {
                    t.rollback();
                }
            } finally {
                if (t != null) {
                    t.commit();
                }
                ad.closeSessionAndTransaction();
            }
        }
        return success;
    }
*/
    public boolean sendMoney(BigDecimal amount, AccountBank sender, AccountBank recipient, String title) {
        boolean success = true;
        
        Date date = new Date();
        //create operation
        FinancialOperation operation = new FinancialOperation();
        operation.setDate(date);
        operation.setTitle(title);
        operation.setAmount(amount);
        operation.setAccount(recipient);

        //insert operation
        FinancialDao fd = new FinancialDao();
        Transaction t = null;
        boolean operationInsterted = false;
        try {
            Session s = fd.openCurrentSession();
            t = s.beginTransaction();
            operation = fd.merge(operation);
            
            operationInsterted = true;
        } catch (Exception e) {
            success = false;
            if (t != null) {
                t.rollback();
            }
        } finally {
            if (t != null) {
                t.commit();
            }
            fd.closeSessionAndTransaction();
        }

        //update account
        if (operationInsterted) {
            BigDecimal actualRecipientFinanceStatus = recipient.getBalance().add(amount);
            recipient.setBalance(actualRecipientFinanceStatus);
            
            BigDecimal actualSenderFinanceStatus = sender.getBalance().subtract(amount);
            sender.setBalance(actualSenderFinanceStatus);
            
            AccountDao ad = new AccountDao();
            t = null;
            try {
                Session s = ad.openCurrentSession();
                t = s.beginTransaction();
                recipient = ad.merge(recipient);
                sender = ad.merge(sender);
                
            } catch (Exception e) {
                success = false;
                if (t != null) {
                    t.rollback();
                }
            } finally {
                if (t != null) {
                    t.commit();
                }
                ad.closeSessionAndTransaction();
            }
        }
        return success;
    }
    
    
    public void setDue(UserModel user, BigDecimal amount, BigDecimal interestRate, PeriodType periodType, String title, Date maturity) {
        AccountBank account = user.getAccount();
        FinancialOperation fo1 = new FinancialOperation(maturity, amount, title, account);
        FinancialDao fd = new FinancialDao();
        AccountDao ad = new AccountDao();

        try {
            Session s = fd.openCurrentSession();
            Transaction t = s.beginTransaction();
            fo1 = fd.merge(fo1);
            account.getFinancialOperations().add(fo1);

            t.commit();

        } catch (Exception e) {
            fd.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally {
            fd.closeSessionAndTransaction();
        }

        try {
            Session s = ad.openCurrentSession();
            Transaction t = s.beginTransaction();
            account = ad.merge(account);
            t.commit();
        } catch (Exception e) {
            ad.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally {
            ad.closeSessionAndTransaction();
        }
    }

    public void cancelDue(FinancialOperation operation) {
        //TODO relation and way of cancelling
        throw new NotImplementedException();
    }

}
