package ekzos.helpers;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ignac_000 on 11/12/2014.
 */
public class Helpers
{
    public static BigDecimal percentageValue(BigDecimal base, BigDecimal percent){
        return base.multiply(percent).divide(new BigDecimal(100));
    }

    public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }
}
