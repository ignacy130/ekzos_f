package ekzos.helpers; /**
 * Created by ignac_000 on 11/12/2014.
 */

/**
 * Zamiana liczby na slowa
 * www.algorytm.org
 * (c)2005 Tomasz Lubinski
 */
public class NumberToWordsConverter
{
    
    private static String jednosci[] = {"", " jeden", " dwa", " trzy", " cztery", " piec", " szesc", " siedem", " osiem", " dziewiec"};
    private static String nascie[] = {"dziesięć", " jedenaście", " dwanaście", " trzynaście", " czternaście", " pietnaście", " szesnaście", " siedemnaście", " osiemnaście", " dziewietnaście"};
    private static String dziesiatki[] = {"", " dziesięć", " dwadzieścia", " trzydzieści", " czterdzieści", " piecdziesiąt", " szescdziesiąt", " siedemdziesiąt", " osiemdziesiąt", " dziewiecdziesiąt"};
    private static String setki[] = {"", " sto", " dwieście", " trzysta", " czterysta", " pięćset", " sześćset", " siedemset", " osiemset", " dziewięćset"};
    private static String x[] = {"", " tys.", " mln.", " mld.", " bln.", " bld."};
    
    public static String Convert(double number)
    {
        
        String slownie = " ";
        int koncowka;
        int rzad = 0;
        int j = 0;
        int minus = 0;

        if (number < 0)
        {
            minus = 1;
            number = -number;
        }
    
        if (number == 0)
        {
            slownie = "zero";
        }
        
        while (number > 0)
        {
            koncowka = (int)(number % 10);
            number /= 10;
            if ((j == 0) && (number % 100 != 0 || koncowka != 0))
            {
                slownie = x[rzad] + slownie;
            }
            if ((j == 0) && (number % 10 != 1))
            {
                slownie = jednosci[koncowka] + slownie;
            }
            if ((j == 0) && (number % 10 == 1))
            {
                slownie = nascie[koncowka] + slownie;
                number /= 10;
                j += 2;
                continue;
            }
            if (j == 1)
            {
                slownie = dziesiatki[koncowka] + slownie;
            }
            if (j == 2)
            {
                slownie = setki[koncowka] + slownie;
                j = -1;
                rzad++;
            }
            j++;
        }
    
        if (minus == 1)
        {
            slownie = "minus " + slownie;
        }
        
        return slownie;
    }
}
