package ekzos.payments.service_contract;

import ekzos.scholarship_type.Scholarship;
import java.util.Date;
/**
 * Created by Paweł on 2014-12-08.
 */
public interface IPaymentComponent {
    String checkIncome(Scholarship scholarship, Date actualDate);
    String getIncome(int accountNumber, Scholarship scholarship);
}
