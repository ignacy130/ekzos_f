package ekzos.payments.service_implementation;

import ekzos.payments.service_contract.IPaymentComponent;
import ekzos.scholarship_type.Scholarship;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Paweł on 2014-12-08.
 */
public class PaymentComponent implements IPaymentComponent
{

    @Override
    public String checkIncome(Scholarship scholarship, Date actualDate)
    {
        //income status
        //amount
        // income date
        //kind of income
        if (scholarship.getAmountGranted().signum()==0)//to refactor
        {
            return "You have Income in an amount" + scholarship.getAmountGranted() + " from type" + scholarship.getScholarshipType() + "received on" + actualDate.toString();
        }

        return "No additional Revenues";
    }

    @Override
    public String getIncome(int accountNumber, Scholarship scholarship)
    {
        // gets income from scholarship
        // deliver to specific account Number
        BigDecimal cashIncome = scholarship.getAmountGranted();
        //command for make transfer ?

        return "Revenues are on account";
    }
}
