package ekzos.scholarship_type;

import java.math.BigDecimal;

/**
 * Created by Paweł on 2014-12-08.
 */
public class Scholarship
{
    private String scholarshipType;
    private String ownerName;
    private String address;
    private BigDecimal amountGranted;

    public Scholarship(String scholarshipType, String ownerName, String address, BigDecimal amountGranted)
    {
        this.scholarshipType = scholarshipType;
        this.ownerName = ownerName;
        this.address = address;
        this.amountGranted = amountGranted;
    }

    //region Accessors
    public String getScholarshipType()
    {
        return this.scholarshipType;
    }

    public void setScholarshipType(String type)
    {
        this.scholarshipType = type;
    }

    public String getOwnerName()
    {
        return this.ownerName;
    }

    public void setOwnerName(String name)
    {
        this.ownerName = name;
    }

    public String getAddress()
    {
        return this.address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public BigDecimal getAmountGranted()
    {
        return this.amountGranted;
    }

    public void setAmountGranted(BigDecimal granted)
    {
        this.amountGranted = granted;
    }
    //endregion
}
