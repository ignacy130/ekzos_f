package com.springapp.mvc;

import ekzos.model.Department.Department;
import ekzos.model.User.UserModel;
import ekzos.model.dao.implementation.DepartmentDao;
import ekzos.model.dao.implementation.UserDaoImpl;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by ignac_000 on 17/1/2015.
 */

@Controller
@RequestMapping("/students")
@SessionAttributes("user")
public class DepartmentController
{
    List<UserModel> userModels;
    UserModel user;
    
    public UserModel getUser(){
        return user;
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView students(@ModelAttribute("user") UserModel userPassed) {
        //Testing data only! TODO: change it to data get by parameter from context
        UserDaoImpl udi = new UserDaoImpl();
        user = userPassed;
        try
        {
            Session s = udi.openCurrentSession();
            Transaction t = s.beginTransaction();
            userModels = udi.findAll();
            
            for(UserModel um:userModels)
            {
                if(um.getEmail().equals(userPassed.getEmail()) && 
                        um.getPassword().equals(userPassed.getPassword())){
                    user = um;
                    break;
                }
            }
            t.commit();
        }catch(Exception e)
        {
            udi.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally{
            udi.closeSessionAndTransaction();
        }    
        
        ModelAndView model = new ModelAndView("students");
        model.addObject("students", userModels);
        model.addObject("user", user);
        return model;
    }
    
    @RequestMapping(value="details", method=RequestMethod.POST, params={"index"})
    public ModelAndView details(@RequestParam(required=true, value = "index") int index) {
        ModelAndView model = new ModelAndView("redirect:/transfer_clerk_single");
        model.addObject("user", user);
        return model;
    }
    
}
