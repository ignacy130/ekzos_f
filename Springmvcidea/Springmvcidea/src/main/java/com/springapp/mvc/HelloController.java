package com.springapp.mvc;

import ekzos.model.User.UserModel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/")
public class HelloController {
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView printWelcome() {
		UserModel user = new UserModel("ignacy130@gmail.com", "pass", true);
		ModelAndView model = new ModelAndView("index");
		model.addObject("user", user);
		return model;
	}
}