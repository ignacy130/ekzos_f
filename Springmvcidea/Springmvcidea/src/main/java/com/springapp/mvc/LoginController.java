package com.springapp.mvc;

import ekzos.model.User.UserModel;
import ekzos.model.User.UserType;
import ekzos.model.dao.implementation.UserDaoImpl;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.lang.Exception;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping("/login")
@SessionAttributes("user")
public class LoginController {

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView login() {
        return new ModelAndView("login", "user", new UserModel());
    }

    @RequestMapping(value = "doLogin", method = RequestMethod.POST)
    public ModelAndView doLogin(@ModelAttribute("user") UserModel user) {
        String email = user.getEmail();
        String password = user.getPassword();

        String target = "redirect:/login";
        
        Transaction t = null;
        UserDaoImpl UserCorrect = new UserDaoImpl();
        try {
            Session s = UserCorrect.openCurrentSession();
            t = s.beginTransaction();
            
            List<UserModel> users = UserCorrect.findAll();
            for (UserModel u : users) {
                UserModel userTemp = u;
                if (userTemp.getEmail().equals(email)) {
                    user = userTemp;
                    break;
                }
            }
            if (password.equals(user.getPassword())) {
                boolean b = user.getType().getRole().equals("Admin");
                target = user.getType().getRole().equals("Admin") ? "redirect:/students" : "redirect:/dashboard";
            }
        } catch (Exception e) {
            UserCorrect.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally {
            if (t != null) {
                t.commit();
            }
            UserCorrect.closeSessionAndTransaction();
        }
        ModelAndView mav = new ModelAndView();
        mav.addObject("user", user);
        mav.setViewName(target);
        return mav;
    }
}
