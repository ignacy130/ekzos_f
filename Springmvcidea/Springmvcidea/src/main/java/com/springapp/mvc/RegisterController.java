package com.springapp.mvc;

import ekzos.email_plugin.implementation.AttachmentDetails;
import ekzos.email_plugin.implementation.EmailHelper;
import ekzos.model.Department.Department;
import ekzos.model.Operation.FinancialOperation;
import ekzos.model.User.AccountBank;
import ekzos.model.User.UserModel;
import ekzos.model.User.UserType;
import ekzos.model.dao.implementation.AccountDao;
import ekzos.model.dao.implementation.DepartmentDao;
import ekzos.model.dao.implementation.UserDaoImpl;
import ekzos.model.dao.implementation.UserTypeDao;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/register")
public class RegisterController {

    UserModel user = new UserModel();

    @Bean
    public UserModel getUser() {
        return user;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView register() {
        return new ModelAndView("register", "user", new UserModel());
    }

    @RequestMapping(value = "send", method = RequestMethod.POST)
    public ModelAndView send(@ModelAttribute("user") UserModel regUser) {
        String email = regUser.getEmail();
        UserModel user = regUser;
        Transaction t = null;
        int accnumber = new Random().nextInt(100);
        String strr = Integer.toString(accnumber);
        AccountBank ab2 = new AccountBank(new BigDecimal(1000), new ArrayList<FinancialOperation>(), strr, null);
        ab2.setBankName("PKO");

        Set<UserModel> allUsers = new HashSet<UserModel>();
        
        AccountDao ad = new AccountDao();
        try {
            Session s = ad.openCurrentSession();
            t = s.beginTransaction();
            
            ab2 = ad.merge(ab2);
        } catch (Exception e) {
            
            ad.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally {
            if (t != null) {
                t.commit();
            }
            ad.closeSessionAndTransaction();
        }
        //set account
        user.setAccount(ab2);

        UserTypeDao utd = new UserTypeDao();
        UserType role = null;
        try {
            Session s = utd.openCurrentSession();
            t = s.beginTransaction();
            role = utd.findAll().get(1);
        } catch (Exception e) {
            utd.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally {
            if (t != null) {
                t.commit();
            }
            utd.closeSessionAndTransaction();
        }

        //set type
        user.setType(role);

        DepartmentDao dd = new DepartmentDao();
        HashSet<Department> userDeps = new HashSet<Department>();
        try {
            Session s = dd.openCurrentSession();
            t = s.beginTransaction();
            List<Department> deps = dd.findAll();

            userDeps.add(deps.get(0));

        } catch (Exception e) {
            dd.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally {
            if (t != null) {
                t.commit();
            }
            dd.closeSessionAndTransaction();
        }
        //set departments
        user.setDepartments(userDeps);

        UserDaoImpl udi = new UserDaoImpl();
        t = null;
        try {
            Session s = udi.openCurrentSession();
            t = s.beginTransaction();
            List<UserModel> users = udi.findAll();
            allUsers = new HashSet<>( udi.findAll());
            Boolean isNew = true;
            for (UserModel u : users) {
                if (u.getEmail().equals(email)) {
                    isNew = false;
                }
            }

            Boolean passwordOK = regUser.getPassword() != null && !regUser.getPassword().isEmpty();
            Boolean albumOK = regUser.getAlbumNumber().length() == 6;
            Boolean depOK = !regUser.getDepartments().isEmpty();

            
            
            if (isNew && passwordOK && albumOK && depOK) {
                user = (UserModel) s.merge(regUser);
            }
        } catch (Exception e) {
            udi.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally {
            if (t != null) {
                t.commit();
            }
            udi.closeSessionAndTransaction();
            
            //wysle email
            EmailHelper eh = new EmailHelper();
            ArrayList<AttachmentDetails> al = new ArrayList<AttachmentDetails>();
            MimeMessage mes =null;
            try {
                mes = eh.createEmail("ekzos", regUser.getEmail(), "Witamy w systemie EKZOS!", "Witamy w systemie EKZOS!", al);
            } catch (MessagingException ex) {
                Logger.getLogger(RegisterController.class.getName()).log(Level.SEVERE, null, ex);
            }
            try{
                eh.sendEmail(mes);
            } catch(javax.mail.MessagingException e){
            
            }
            
        }
        //allUsers
        /*
        dd = new DepartmentDao();
        allUsers.add(regUser);
        t = null;
        try{
            Session s = dd.openCurrentSession();
            t = s.beginTransaction();
            Department deps = dd.findAll().get(0);
            deps.setUserList(allUsers);
            deps = dd.merge(deps);
        }
        catch(Exception e){
            t.rollback();
        }
        finally{
            if(t!=null)
            t.commit();
            dd.closeSessionAndTransaction();
            
            
            
            
        }
         */       

        ModelAndView mav = new ModelAndView("redirect:/login");
        mav.addObject("user", user);
        return mav;
    }

}
