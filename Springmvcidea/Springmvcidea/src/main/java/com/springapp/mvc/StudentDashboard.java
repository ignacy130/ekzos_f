package com.springapp.mvc;

import ekzos.model.Operation.FinancialOperation;
import ekzos.model.User.UserModel;
import ekzos.model.dao.implementation.FinancialDao;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Mateusz
 */

@Controller
@RequestMapping("/dashboard")
@SessionAttributes("user")
public class StudentDashboard {
    UserModel user;
    
    public UserModel getUser(){
        return user;
    }
    
    @RequestMapping(value="transfer", method=RequestMethod.POST)
    public ModelAndView transfer(@ModelAttribute("user") UserModel user) {
        ModelAndView model = new ModelAndView("redirect:/transfer_student");
        model.addObject("user", user);
        return model;
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView students(@ModelAttribute("user") UserModel userPassed)
    {
        ModelAndView model = new ModelAndView("dashboard_student");
        
        ArrayList<FinancialOperation> operations = new ArrayList<FinancialOperation>();
        FinancialDao fd = new FinancialDao();
        Transaction t = null;
        try {
            Session s = fd.openCurrentSession();
            t = s.beginTransaction();
            
            List<FinancialOperation> ops = fd.findAll();
            for(FinancialOperation op : ops){
                if(op.getAccount().getNumber().equals(userPassed.getAccount().getNumber())){
                    operations.add(op);
                }
            }
            
        } catch (Exception e) {
            fd.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally {
            if (t != null) {
                t.commit();
            }
            fd.closeSessionAndTransaction();
        }
        
        userPassed.getAccount().setFinancialOperations(operations);
        
        model.addObject("user", userPassed);
        return model;
    }
    
    
}