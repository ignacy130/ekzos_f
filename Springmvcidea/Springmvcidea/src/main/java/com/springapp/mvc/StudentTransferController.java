package com.springapp.mvc;

import com.springapp.mvc.models.TransferViewModel;
import ekzos.fees.implementation.FeesComponent;
import ekzos.model.User.AccountBank;
import ekzos.model.User.UserModel;
import ekzos.model.dao.implementation.UserDaoImpl;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes("user")
@RequestMapping("/transfer_student")
public class StudentTransferController {
    
    UserModel user;
    
    public UserModel getUser(){
        return user;
    }       

    private TransferViewModel tvm = new TransferViewModel();

    @ModelAttribute("tvm")
    public TransferViewModel getTvm() {
        return tvm;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView StudentTransfer(@ModelAttribute("user") UserModel userPassed) {

        ModelAndView model = new ModelAndView("transfer_student");

        UserDaoImpl udao = new UserDaoImpl();
        UserModel dep = null;
            Transaction t = null;
            try {
                Session s = udao.openCurrentSession();
                t = s.beginTransaction();

                for (UserModel u : udao.findAll()) {
                    if (u.getType().getRole().equals("Admin")) {
                        dep = u;
                        break;
                    }
                }
                
            } catch (Exception e) {
                udao.getSessionFactory().getCurrentSession().getTransaction().rollback();
            } finally {
                if (t != null) {
                    t.commit();
                }
                udao.closeSessionAndTransaction();
            }
        
        this.tvm = new TransferViewModel();
        this.tvm.number = dep.getAccount().getNumber();
        model.addObject("tvm", this.tvm);
        this.user = userPassed;
model.addObject("user", user);
        return model;
    }

    @RequestMapping(value = "realizeOperation", method = RequestMethod.POST)
    public String realizeOperation(@ModelAttribute("tvm") TransferViewModel tvm, BindingResult result) {

        System.out.println("Operacja przeprowadzona");
        AccountBank sender = user.getAccount();
        if (tvm.isValid()) {
            //find account
            UserDaoImpl udao = new UserDaoImpl();
            Transaction t = null;
            AccountBank recipient = null;
            try {
                Session s = udao.openCurrentSession();
                t = s.beginTransaction();

                for (UserModel u : udao.findAll()) {
                    AccountBank accountTemp = u.getAccount();
                    if (accountTemp.getNumber().equals(tvm.getRecipientNumber())) {
                        recipient = accountTemp;
                        break;
                    }
                }
                
            } catch (Exception e) {
                udao.getSessionFactory().getCurrentSession().getTransaction().rollback();
            } finally {
                if (t != null) {
                    t.commit();
                }
                udao.closeSessionAndTransaction();
            }
            
            //do operation
            boolean success = FeesComponent.getFeesComponent().sendMoney(tvm.getAmount(), sender, recipient, tvm.getTitle());
        }
        return "redirect:/transfer_student";
    }

}