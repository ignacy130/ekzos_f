package com.springapp.mvc;

import com.springapp.mvc.models.TransferViewModel;
import ekzos.fees.implementation.FeesComponent;
import ekzos.model.Department.Department;
import ekzos.model.Operation.FinancialOperation;
import ekzos.model.User.AccountBank;
import ekzos.model.User.UserModel;
import ekzos.model.dao.implementation.FinancialDao;
import ekzos.model.dao.implementation.UserDaoImpl;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import static org.springframework.http.RequestEntity.method;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes("user")
@RequestMapping("/transfer_clerk_single")
public class TransferSingleController {

    UserModel user;
    
    public UserModel getUser(){
        return user;
    }  
    
    private TransferViewModel tvm = new TransferViewModel();

    @ModelAttribute("tvm")
    public TransferViewModel getTvm() {
        return tvm;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView TransferSingle(@ModelAttribute("user") UserModel user) {
        //Testing data only! TODO: change it to data get by parameter from context
        Set<UserModel> sum = new HashSet<UserModel>() {
        };
        this.user = user;
        UserModel activeUser = user;
        UserDaoImpl udi = new UserDaoImpl();
        try {
            Session s = udi.openCurrentSession();
            Transaction t = s.beginTransaction();


            for (UserModel d : udi.findAll()) {
                
                    sum.add(d);
                
            }

            t.commit();
        } catch (Exception e) {
            udi.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally {
            udi.closeSessionAndTransaction();
        }

        ModelAndView model = new ModelAndView("transfer_clerk_single");

        model.addObject("students", sum);
        model.addObject("user", user);
        model.addObject("tvm", new TransferViewModel());

        return model;
    }

    @RequestMapping(value = "realizeOperation", method = RequestMethod.POST)
    public String realizeOperation(@ModelAttribute("tvm") TransferViewModel tvm, BindingResult result) {

        System.out.println("Operacja przeprowadzona");

        if (tvm.isValid()) {
            //find account
            UserDaoImpl udao = new UserDaoImpl();
            Transaction t = null;
            AccountBank sender = null; //actually its recipient
            try {
                Session s = udao.openCurrentSession();
                t = s.beginTransaction();

                for (UserModel u : udao.findAll()) {
                    AccountBank accountTemp = u.getAccount();
                    if (accountTemp.getNumber().equals(tvm.getRecipientNumber())) {
                        sender = accountTemp;
                        break;
                    }
                }
                
            } catch (Exception e) {
                udao.getSessionFactory().getCurrentSession().getTransaction().rollback();
            } finally {
                if (t != null) {
                    t.commit();
                }
                udao.closeSessionAndTransaction();
            }
            AccountBank recipient = user.getAccount(); //actually its sender
            //do operation
            boolean success = FeesComponent.getFeesComponent().sendMoney(tvm.getAmount(), recipient, sender, tvm.getTitle());
        }
        return "redirect:/transfer_clerk_single";
    }

}