/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapp.mvc.models;

import ekzos.model.Operation.FinancialOperation;
import ekzos.model.User.UserModel;
import java.math.BigDecimal;

/**
 *
 * @author Mateusz
 */
public class TransferViewModel{
	public String recipientName;
	public String recipientAddress;
	public BigDecimal amount;
        public String number;
        public Boolean isPlus;
        public String title;
        
        public String getRecipientName(){
            return recipientName;
        }
        public void setRecipientName(String val){
            recipientName = val;
        }
        
        public String getTitle(){
            return title;
        }
        public void setTitle(String val){
            title = val;
        }
        
        public String getRecipientAddress(){
            return recipientAddress;
        }
        public void setRecipientAddress(String val){
            recipientAddress = val;
        }
        
        public String getRecipientNumber(){
            return number;
        }
        public void setRecipientNumber(String val){
            number = val;
        }
        
        public BigDecimal getAmount(){
            return amount;
        }
        public void setAmount(BigDecimal val){
            amount = val;
        }

        public Boolean getIsPlus(){
            return isPlus;
        }
        public void setIsPlus(Boolean val){
            isPlus = val;
        }

        public Boolean isValid(){
            Boolean numberOk = !number.isEmpty();
            Boolean amountOk = amount.abs()!=BigDecimal.ZERO;
            Boolean titleOk = !title.isEmpty();
            Boolean recipientOk = !recipientName.isEmpty();
            return numberOk && amountOk && titleOk && recipientOk;
        }
        
    public TransferViewModel() {
    }

        
        
    public TransferViewModel(String recipientName, String recipientAddress, BigDecimal amount, String recipientNumber, Boolean isPlus, String title) {
        this.recipientName = recipientName;
        this.recipientAddress = recipientAddress;
        this.amount = amount;
        this.number = recipientNumber;
        this.isPlus = isPlus;
        this.title = title;
    }

}
