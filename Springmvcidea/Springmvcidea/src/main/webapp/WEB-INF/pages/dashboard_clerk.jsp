<!DOCTYPE html>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html lang="en">
    <jsp:include page="/WEB-INF/pages/head.jsp"/>
    <body>
        <jsp:include page="/WEB-INF/view/header.jsp"/>
        <div class="container">
            <div class="row">
                <!-- operations -->
                <div class="col-md-12">
                    <jsp:include page="/WEB-INF/view/operations_view.jsp"/>
                </div>
                <!-- operations end -->           
            </div>
        </div>
        <jsp:include page="/WEB-INF/view/footer.jsp"/>
    </body>
</html>