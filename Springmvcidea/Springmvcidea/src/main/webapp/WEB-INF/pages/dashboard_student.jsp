<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
    <jsp:include page="/WEB-INF/pages/head.jsp"/>
    <body>
        <jsp:include page="/WEB-INF/pages/header.jsp"/>

        <!-- Main -->
        <div class="container">

            <div class="row">
                <div class="col-md-4">
                    <!-- saldo -->

                    <div >
                        <h2 class="text-center">
                            Saldo: ${user.account.balance}
                        </h2>
                        <form:form method="post" action="dashboard/transfer">
                            <input class="btn btn-lg btn-warning btn-block" type="submit" value="Wyslij!" ></input>
                        </form:form>
                    </div>
                    <!-- saldo end -->
                    
                </div>

                <!-- operations -->
                <div class="col-md-8">
                   <jsp:include page="/WEB-INF/pages/operations_view.jsp"/>
                </div>
            </div>
            <!-- operations end -->           
        </div>




    </div><!--/container-->
<jsp:include page="/WEB-INF/pages/footer.jsp"/>
</body>
</html>