<%-- 
    Document   : footer
    Created on : Jan 18, 2015, 11:45:03 AM
    Author     : ignac_000
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="clearfix"></div>
<footer class="text-center">
    <div>
        Autorzy: Paweł Żurawik, Mateusz Chodkowski, Ignacy Wilczyński
    </div>
    This Bootstrap 3 dashboard layout is compliments of
    <a href="http://www.bootply.com/85850">
        <strong>
            Bootply.com
        </strong>
    </a>
</footer>
        <!-- script references -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
