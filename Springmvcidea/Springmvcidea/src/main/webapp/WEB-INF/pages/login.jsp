<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html lang="en">
    <jsp:include page="/WEB-INF/pages/head.jsp"/>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 jumbotron text-center">
                    <h1>EKZOS</h1>
                    <div class="lead"> Twoj elektroniczny system studenta</div>
                    <div class="lead">logowanie</div>
                    <form:form class="form-signin" commandName="user"  method="post" name="user" action="login/doLogin">

                        <label for="inputEmail" class="sr-only">Email address</label>
                        <form:input id="inputEmail" class="form-control" placeholder="EmailAddress" autofocus="" path="email" type="email"></form:input>
                        <label for="inputPassword" class="sr-only">Password</label>
                        <form:input id="inputPassword" class="form-control" placeholder="Password" path="password" type="password"></form:input>

                        <!--<a href="dashboard.html">
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                        </a>-->
                        <br/>

                        <input class="btn btn-lg btn-primary btn-block"  type="submit" action="students.html"  value="Wyslij!" />
                    </form:form>

                </div>
            </div>
        </div>
<script src=index-files/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>