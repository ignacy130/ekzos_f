<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html lang="en">
<jsp:include page="/WEB-INF/pages/head.jsp"/>
<body>

<div class="container">
  <div class="row">
    <div class="col-md-offset-2 col-md-8 jumbotron text-center">
      <h1>EKZOS</h1>
      <div class="lead">Twoj elektroniczny system studenta</div>
      <div class="lead">Rejestracja</div>
      <form:form commandName="user" class="form-signin" method="post" name="user" action="register/send">
        <label for="inputEmail" class="sr-only">Numer albumu</label>
        <form:input id="inputEmail" class="form-control" placeholder="Numer albumu" autofocus="" aria-describedby="helpBlock" path="albumNumber"></form:input>

        <span id="helpBlock" class="help-block">Znajduje sie na Twojej legitymacji, ma 6 cyfr.</span>
        <form:input class="form-control" type="email" path="email" ></form:input>
        <label for="inputEmail" class="sr-only">Adres email</label>


        <span id="helpBlock" class="help-block">Adres, ktory podawales przy rejestracji na studia.</span>

        <label for="inputPassword" class="sr-only">Haslo</label>
        <form:input id="inputPassword" class="form-control" placeholder="Haslo" type="password" aria-describedby="helpBlock" path="password"></form:input>

        <span id="helpBlock" class="help-block">Haslo - pamietaj, aby bylo bezpieczne!</span>
        <div class="checkbox">
          <label>
            <input value="show-password" type="checkbox"> Pokaz haslo
            <c:out value = "${password}" />
          </label>
        </div>

        <label for="selectDepartment" class="sr-only">Wydzia?</label>
        <select id="selectDepartment" class="form-control">
          <option>Wybierz wydzia?...</option>
          <option>WZIM</option>
          <option>WIP</option>
          <option>WNE</option>
        </select>

        <span id="helpBlock" class="help-block">Wydzial do ktorego jestes przypisany.</span>

        <br/>
        <input class="btn btn-lg btn-primary btn-block"  type="submit" action="send.html"  value="Wyslij!" />
        <br/>

        <button class="btn btn-info btn-block">Logowanie</button>
      </form:form>
    </div>
  </div>
</div> <!-- /container -->


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="index_files/ie10-viewport-bug-workaround.js"></script>


</body></html>