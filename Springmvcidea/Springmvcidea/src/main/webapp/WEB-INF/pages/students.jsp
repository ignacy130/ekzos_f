<!DOCTYPE html>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html lang="en">
    <jsp:include page="/WEB-INF/pages/head.jsp"/>
    <body>
        <jsp:include page="/WEB-INF/pages/header.jsp"/>
        <!-- Main -->
        <div class="container">
            <div class="row">
                <!-- students -->
                <div class="col-md-12">
                    <jsp:include page="/WEB-INF/pages/students_view.jsp"/>
                </div>
                <!-- students end -->
            </div>
        </div>
    </div><!--/container-->
    <!-- /Main -->


   <jsp:include page="/WEB-INF/pages/footer.jsp"/>
    
</body>
</html>