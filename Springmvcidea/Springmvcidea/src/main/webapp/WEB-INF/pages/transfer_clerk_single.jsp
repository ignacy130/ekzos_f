<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html lang="en">
 <jsp:include page="/WEB-INF/pages/head.jsp"/>
    <body>
      <!-- Header -->
       <jsp:include page="/WEB-INF/pages/header.jsp"/>
     <!-- /Header -->

     <!-- Main -->

   </div>
   <!-- /span-3 -->
   <!-- user menu end -->

   <div class="container">


     <div class="col-md-6">
      <!-- operations-->
      <div class="col-md-12">
        <jsp:include page="/WEB-INF/pages/students_view.jsp"/>
      </div>
      <!-- operations end -->
    </div>


    <div class="col-md-6">
      <h2>Transfer</h2>
      <small>wszystkie pola sa obowiazkowe</small>
      <form:form commandName="tvm" name="tvm" class="form" method="post" action="transfer_clerk_single/realizeOperation">

        <div class="radio">
          <label style="border-right: 1px solid #ddd; padding-right: 10px; margin-right: 5px;">
              <form:radiobutton path="isPlus"  name="typeRadios" id="optionsRadios1" value="true" checked="true" ></form:radiobutton>
            Uznanie
          </label>
          <label>
            <form:radiobutton path="isPlus"  name="typeRadios" id="optionsRadios1" value="false"></form:radiobutton>
            Obci??enie
          </label>
        </div>

        <label for="inputName" class="sr-only">Numer rachunku nadawcy</label>
        <input id="inputName" class="form-control" placeholder="Numer rachunku nadawcy" autofocus="" type="text" aria-describedby="helpBlock" value=${user.account.number} disabled>
        <span id="helpBlock" class="help-block">Twoj numer rachunku. Mozesz go zmienic w <a href="#"> ustawieniach.</a></span>

        <label for="inputName" class="sr-only">Nazwa / godnosc</label>
        <form:input id="inputName" path="recipientName" class="form-control" placeholder="Nazwa / godnosc" autofocus="" type="text" aria-describedby="helpBlock" ></form:input>
        <span id="helpBlock" class="help-block">Nazwa instytucji / imie i nazwisko odbiorcy</span>

        <label for="inputAmount" class="sr-only">Kwota</label>
        <form:input path="amount" id="inputAmount" class="form-control" placeholder="Kwota" autofocus="" type="text" aria-describedby="helpBlock" ></form:input>
        <span id="helpBlock" class="help-block">Kwota transakcji</span>

        <label for="inputNumber" class="sr-only">Numer rachunku odbiorcy</label>
        <form:input path="recipientNumber" id="inputNumber" class="form-control" placeholder="Numer rachunku odbiorcy" type="text" aria-describedby="helpBlock" required="true"  ></form:input>
        <span id="helpBlock" class="help-block">Numer rachunku na ktory chcesz wyslac pieniadze</span>

        <label for="inpuTitle" class="sr-only">Tytul przelewu</label>
        <form:input path="title" id="inputTitle" class="form-control" placeholder="Tytul przelewu" type="text" aria-describedby="helpBlock"></form:input>
        <span id="helpBlock" class="help-block">Tytul Twojego przelewu, zalecamy podanie numeru operacji/faktury</span>

        <br/>
            <input name="submit" class="btn btn-lg btn-primary btn-block"  type="submit" onclick="location.href='transfer_clerk_single/realizeOperation.html'" value="Zrealizuj przelew!" />
            <!--<input class="btn btn-lg btn-primary btn-block" type="submit" value="Zrealizuj przelew" >-->
          <!--<button class="btn btn-lg btn-primary btn-block disabled" type="submit" >Zrealizuj przelew</button>-->
        <br/>
  
      </form:form>
    </div>


  </div>

</div><!--/container-->
<!-- /Main -->

<div class="clearfix"></div>
 <jsp:include page="/WEB-INF/pages/footer.jsp"/>

</body>
</html>