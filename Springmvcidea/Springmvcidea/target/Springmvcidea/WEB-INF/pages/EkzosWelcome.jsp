<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
    <jsp:include page="/WEB-INF/pages/head.jsp"/>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 jumbotron text-center">
                    <h1>EKZOS</h1>
                    <div class="lead">Tw�j elektroniczny system studenta</div>
                    <div class="lead">Logowanie</div>
                    <form class="form-signin">

                        <label for="inputEmail" class="sr-only">Email address</label>

                        <input id="inputEmail" class="form-control" placeholder="Email address" autofocus="" type="email">

                        <label for="inputPassword" class="sr-only">Password</label>

                        <input id="inputPassword" class="form-control" placeholder="Password" type="password">

                        <a href="dashboard.html">
                            <button class="btn btn-lg btn-primary btn-block" type="submit" >Sign in</button>
                        </a>
                        <br/>

                        <button class="btn btn-info btn-block">Rejestracja</button>
                    </form>
                </div>
            </div>
        </div> <!-- /container -->

        <jsp:include page="/WEB-INF/pages/footer.jsp"/>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="index_files/ie10-viewport-bug-workaround.js"></script>

    </body>
</html>