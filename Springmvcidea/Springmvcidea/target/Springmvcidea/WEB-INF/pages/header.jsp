<%-- 
    Document   : header
    Created on : Jan 17, 2015, 2:57:22 PM
    Author     : ignac_000
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <!-- toggler -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">EKZOS</a>
        </div>
        <!-- toggler end-->

        <!--menu-->
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="#">Studenci</a></li>
                <li><a href="#">Operacje</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="active">
                    <a>
                        <span>${user.email} (${user.type.role})</span>
                    </a>
                </li>
                <li><a href="#">Ustawienia</a></li>
                <li><a href="http://ekzos.azurewebsites.net/Springmvcidea/login">Wyloguj</a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->

    </div>
</div>