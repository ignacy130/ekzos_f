<!DOCTYPE html>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="http://getbootstrap.com/favicon.ico">

<title>EKZOS - Logowanie</title>

<!-- Bootstrap core CSS -->
<link href="/resources/css/bootstrap.css" /> rel="stylesheet">

<!-- Custom styles for this template -->
<link href="/resources/css/signin.css" rel="stylesheet">
<link href="resources/css/styles.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="index_files/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
<form:form modelAttribute="user">
    <body style="padding: 0;">
      <h1>
        <form:input path="email"></form:input>
      </h1>
      <h1>
          ${user.email}
      </h1>
      <div class="container">
      <div class="row">
          <div class="col-md-offset-2 col-md-8 jumbotron text-center" style="padding-top: 10px;">
            <h1><span class='brand'>EKZOS</span> SGGW</h1>
            <form class="form-signin">
              
              <label for="inputEmail" class="sr-only">Adres email (login)</label>
              <input id="inputEmail" class="form-control" placeholder="Adres email (login)" autofocus="" type="email">

              <label for="inputPassword" class="sr-only">Hasło</label>
              <input id="inputPassword" class="form-control" placeholder="Hasło" type="password">
              
              <a href="dashboard.html">
                <button class="btn btn-lg btn-primary btn-block" type="submit" >Zaloguj się</button>
              </a>
              <br/>
              <hr/>
              <span id="helpBlock" class="help-block">Nie pamiętasz hasła? Podaj swój email</span>
              <label for="inputRecover" class="sr-only">Nie pamiętasz hasła? Podaj swój email</label>
              <input id="inputRecover" class="form-control" placeholder="Adres email" autofocus="" type="email" aria-describedby="helpBlock">
              <button class="btn btn-info btn-block">Resetuj hasło</button>

              <hr/>
              <span id="helpBlock" class="help-block">Nie jesteś jeszcze zarejestrowany?</span>
              <label for="inputRecover" class="sr-only">Nie jesteś jeszcze zarejestrowany?</label>
              <button class="btn btn-info btn-block">Zarejestruj się!</button>
            </form>
          </div>
        </div>
      </div> <!-- /container -->
</form:form>

      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="index_files/ie10-viewport-bug-workaround.js"></script>


    </body></html>