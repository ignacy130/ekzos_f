<%-- 
    Document   : operations_view
    Created on : Jan 17, 2015, 2:20:53 PM
    Author     : ignac_000
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<form:form>
    <h2>Operacje</h2>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="col-md-4">Data</th>
                <th class="col-md-2">Kwota</th>
                <th>Tytul</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${user.account.financialOperations}" var="operation">
                <tr>
                    <td>${operation.date}</td>
                    <td>${operation.amount} zl</td>
                    <td>${operation.title}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</form:form>
