<!DOCTYPE html>
<html lang="en">
    <jsp:include page="/WEB-INF/pages/head.jsp"/>
    <body>
        <jsp:include page="/WEB-INF/pages/header.jsp"/>

        <div class="container">
            <div class="col-md-6">
                <jsp:include page="/WEB-INF/view/students_view.jsp"/>
            </div>
        </div>

        <div class="col-md-6">
            <h2>Transfer</h2>
            <form:form class="form">

                <c:if test="${user.type.getRole() != Clerk.toString()}">
                    <div class="radio">
                        <label style="border-right: 1px solid #ddd; padding-right: 10px; margin-right: 5px;">
                            <form:input type="radio" name="typeRadios" id="optionsRadios1" value="option1"  checked>
                                Uznanie
                        </label>
                        <label>
                            <form:input type="radio" name="typeRadios" id="optionsRadios1" value="option1">
                                Obciazenie
                        </label>
                    </div>
                </c:if>

                <h1>TRANSFERVIEWMODEL!</h1>
                <label for="inputName" class="sr-only">Numer rachunku nadawcy</label>
                <form:label path="sender.account.number" path="" id="inputName" class="form-control" placeholder="Numer rachunku nadawcy" autofocus="" type="text" aria-describedby="helpBlock" disabled>
                    <span id="helpBlock" class="help-block">Twoj numer rachunku. Mozesz go zmienic w <a href="#"> ustawieniach.</a></span>

                    <label for="inputAmount" class="sr-only">Kwota</label>
                    <form:input path="amount" id="inputAmount" class="form-control" placeholder="Kwota" autofocus="" type="text" aria-describedby="helpBlock">
                        <span id="helpBlock" class="help-block">Kwota transakcji</span>

                        <label for="inputAddress" class="sr-only">Adres</label>
                        <form:input id="inputAddress" class="form-control" placeholder="Adres fizyczny" autofocus="" type="text" aria-describedby="helpBlock">
                            <span id="helpBlock" class="help-block">Adres korespondencyjny odbiorcy.</span>

                            <label for="inputNumber" class="sr-only">Numer rachunku odbiorcy</label>
                            <form:input path="receiver.account.number" id="inputNumber" class="form-control" placeholder="Numer rachunku odbiorcy" type="text" aria-describedby="helpBlock" required>
                                <span id="helpBlock" class="help-block">Numer rachunku na ktory chcesz wyslac pieniadze</span>

                                <label for="inpuTitle" class="sr-only">Tytul przelewu</label>
                                <form:input path="transferTitle" id="inputTitle" class="form-control" placeholder="Tytul przelewu" type="text" aria-describedby="helpBlock">
                                    <span id="helpBlock" class="help-block">Tytul Twojego przelewu, zalecamy podanie numeru operacji/faktury</span>

                                    <br/>
                                    <a href="dashboard.html">
                                        <button class="btn btn-lg btn-primary btn-block disabled" type="submit" >Zrealizuj przelew</button>
                                    </a>
                                    <br/>
                                    </form:form>
                                    </div>


                                    </div>

                                    </div><!--/container-->
                                    <!-- /Main -->
                                    <jsp:include page="/WEB-INF/pages/footer.jsp"/>
                                    </body>
                                    </html>