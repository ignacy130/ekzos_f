<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
 <jsp:include page="/WEB-INF/pages/head.jsp"/>
    <body>
      <!-- Header -->
       <jsp:include page="/WEB-INF/pages/header.jsp"/>


   <div class="col-md-4">
    <!-- operations-->
    <div class="col-md-12 center">
      <div class="table-responsive">
        <jsp:include page="/WEB-INF/pages/operations_view.jsp"/>
      </div>
      <!-- operations end -->
    </div>
    <!-- operations end -->
  </div>


  <div class="col-md-8">
    <h2>Transfer</h2>
<small>wszystkie pola są obowiązkowe</small>

    <form:form commandName="tvm" name="tvm" class="form" method="post" action="transfer_student/realizeOperation">

        <div class="radio">
          <label style="border-right: 1px solid #ddd; padding-right: 10px; margin-right: 5px;">
              <form:radiobutton path="isPlus"  name="typeRadios" id="optionsRadios1" value="true" checked="true" ></form:radiobutton>
            Uznanie
          </label>
          <label>
            <form:radiobutton path="isPlus"  name="typeRadios" id="optionsRadios1" value="false"></form:radiobutton>
            Obci??enie
          </label>
        </div>

        <label for="inputName" class="sr-only">Numer rachunku nadawcy</label>
        <input id="inputName" class="form-control" placeholder="Numer rachunku nadawcy" autofocus="" type="text" aria-describedby="helpBlock" value=${user.account.number} disabled>
        <span id="helpBlock" class="help-block">Twoj numer rachunku. Mozesz go zmienic w <a href="#"> ustawieniach.</a></span>

        <label for="inputName" class="sr-only">Nazwa / godnosc</label>
        <form:input id="inputName" path="recipientName" class="form-control" placeholder="Nazwa / godnosc" autofocus="" type="text" aria-describedby="helpBlock" ></form:input>
        <span id="helpBlock" class="help-block">Nazwa instytucji / imie i nazwisko odbiorcy</span>

        <label for="inputAmount" class="sr-only">Kwota</label>
        <form:input path="amount" id="inputAmount" class="form-control" placeholder="Kwota" autofocus="" type="text" aria-describedby="helpBlock" ></form:input>
        <span id="helpBlock" class="help-block">Kwota transakcji</span>

        <label for="inputNumber" class="sr-only">Numer rachunku odbiorcy</label>
        <form:input path="recipientNumber" id="inputNumber" class="form-control" placeholder="Numer rachunku odbiorcy" type="text" aria-describedby="helpBlock" required="true"  ></form:input>
        <span id="helpBlock" class="help-block">Numer rachunku na ktory chcesz wyslac pieniadze</span>

        <label for="inpuTitle" class="sr-only">Tytul przelewu</label>
        <form:input path="title" id="inputTitle" class="form-control" placeholder="Tytul przelewu" type="text" aria-describedby="helpBlock"></form:input>
        <span id="helpBlock" class="help-block">Tytul Twojego przelewu, zalecamy podanie numeru operacji/faktury</span>

        <br/>
            <input name="submit" class="btn btn-lg btn-primary btn-block"  type="submit" onclick="location.href='transfer_clerk_single/realizeOperation.html'" value="Zrealizuj przelew!" />
            <!--<input class="btn btn-lg btn-primary btn-block" type="submit" value="Zrealizuj przelew" >-->
          <!--<button class="btn btn-lg btn-primary btn-block disabled" type="submit" >Zrealizuj przelew</button>-->
        <br/>
  
      </form:form>
    <br/>
  
</div>




</div><!--/container-->
<!-- /Main -->

<div class="clearfix"></div>
<footer class="text-center">This Bootstrap 3 dashboard layout is compliments of <a href="http://www.bootply.com/85850"><strong>Bootply.com</strong></a></footer>


<div class="modal" id="addWidgetModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Ã</button>
        <h4 class="modal-title">Add Widget</h4>
      </div>
      <div class="modal-body">
        <p>Add a widget stuff here..</p>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn">Close</a>
        <a href="#" class="btn btn-primary">Save changes</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dalog -->
</div><!-- /.modal -->




<!-- script references -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>