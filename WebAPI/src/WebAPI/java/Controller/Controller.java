package Controller;

/**
 * Created by ignac_000 on 6/1/2015.
 */

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.concurrent.atomic.AtomicLong;

@RequestMapping( value = "/webapi" )
public class Controller {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping(value = "/getUser", method = RequestMethod.GET)
    public String test(@RequestParam(value="id", defaultValue="DefaultName") String name) {
        return "test ok";
    }

    /*
    @RequestMapping(value = "/getUser", method = RequestMethod.GET)
    public UserModel getUser(@RequestParam(value="id", defaultValue="DefaultName") String name) {
        throw new NotImplementedException(); //czekamy na hibernate
        return new UserModel(counter.incrementAndGet(), String.format(template, name));
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public UserModel getUser(@RequestParam(value="id", defaultValue="DefaultName") String name) {
        throw new NotImplementedException(); //czekamy na hibernate
        return new UserModel(counter.incrementAndGet(), String.format(template, name));
    }

    @RequestMapping(value = "/register", method = RequestMethod.PUT)
    public UserModel getUser(@RequestParam(value="id", defaultValue="DefaultName") String name) {
        throw new NotImplementedException(); //czekamy na hibernate
        return new UserModel(counter.incrementAndGet(), String.format(template, name));
    }

    @RequestMapping("/getDues")
    public UserModel getUser(@RequestParam(value="id", defaultValue="DefaultName") String name) {
        throw new NotImplementedException(); //czekamy na hibernate
        return new UserModel(counter.incrementAndGet(), String.format(template, name));
    }

    @RequestMapping("/getScholarships")
    public UserModel getUser(@RequestParam(value="id", defaultValue="DefaultName") String name) {
        throw new NotImplementedException(); //czekamy na hibernate
        return new UserModel(counter.incrementAndGet(), String.format(template, name));
    }

    @RequestMapping("/getUser")
    public UserModel getUser(@RequestParam(value="id", defaultValue="DefaultName") String name) {
        throw new NotImplementedException(); //czekamy na hibernate
        return new UserModel(counter.incrementAndGet(), String.format(template, name));
    }
*/
}