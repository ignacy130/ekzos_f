package Controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
/**
 * Created by ignac_000 on 6/1/2015.
 */
@ComponentScan
@EnableAutoConfiguration
public class WebAPI
{
    public static void main(String[] args) {
        SpringApplication.run(WebAPI.class, args);
    }
}
