import ekzos.implementation.AttachmentDetails
import ekzos.implementation.EmailHelper
import org.junit.Test
import javax.mail.Multipart
import javax.mail.internet.MimeMessage

/**
 * Created by ignac_000 on 30/11/2014.
 */
class EmailHelperTest extends GroovyTestCase {
    @Test
    void testCreateEmail() {
        String sender = "ignacy130@gmail.com";
        String recipient = "chotkos@gmail.com";
        String title = "testEmail";
        String contentText = "Bądź waść gej, chroń kumpli! Złóż syf, tnę!!@#\$%^&*()_+";
        List<AttachmentDetails> attachmentDetails = new ArrayList<AttachmentDetails>();
        MimeMessage message = EmailHelper.createEmail(sender,recipient,title,contentText,attachmentDetails);
        assertEquals(message.sender.toString(), sender);
        assertEquals(message.allRecipients[0].toString(), recipient);
        assertEquals(message.subject.toString(), title);
        //assertEquals(((Multipart)message.getContent()).getBodyPart(0).text, contentText);
        //assertEquals(message.content);
    }
}
