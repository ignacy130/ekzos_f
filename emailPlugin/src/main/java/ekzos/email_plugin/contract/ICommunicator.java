package ekzos.email_plugin.contract;

import ekzos.email_plugin.implementation.AttachmentDetails;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;

/**
 * Created by Mateusz on 2014-11-24.
 * Zmienna remindForMe - nie pamiętam do czego była, nie chcę jej pochopnie usunąć - do ustalenia
 * Designed to be implemented as threadsafe Singletone: http://en.wikipedia.org/wiki/Initialization-on-demand_holder_idiom
 */
public interface ICommunicator{
    MimeMessage createEmail(String senderAddress, String recipientAddress, String title, String textOfMessage, List<AttachmentDetails> attachmentList) throws MessagingException;
    void sendRecoveryLink(String emailAddress,String senderAddress,String recoveryLink);
    void sendEmail(String senderAddress, String recipientAddress, String title, String textOfMessage, List<AttachmentDetails> attachmentList) throws MessagingException;
}
