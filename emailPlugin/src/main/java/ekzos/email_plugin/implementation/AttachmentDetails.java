package ekzos.email_plugin.implementation;

/**
 * Created by Mateusz on 2014-11-24.
 */
public class AttachmentDetails {
    public String fileName;
    public javax.mail.internet.MimeBodyPart attachmentBody;
}
