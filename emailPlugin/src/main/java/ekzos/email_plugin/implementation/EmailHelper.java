package ekzos.email_plugin.implementation;
import ekzos.email_plugin.contract.ICommunicator;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.List;
import java.util.Properties;

/**
 * Created by Mateusz on 2014-11-24.
 */
//@Component
public class EmailHelper implements ICommunicator
{
    private static class Holder {
        static final EmailHelper INSTANCE = new EmailHelper();
    }

    public static EmailHelper getInstance() {
        return Holder.INSTANCE;
    }

    /**
     * Creates an email.
     *
     * @param senderAddress   - Sender address
     * @param recipientAddress - Receiver address
     * @param title           - Email title
     * @param textOfMessage   - Email content
     * @param attachmentList  - List of email attachments
     */

    public MimeMessage createEmail(String senderAddress, String recipientAddress, String title, String textOfMessage, List<AttachmentDetails> attachmentList) throws MessagingException
    {
        /* ORYGINALNE PROPSY OD ZEWN�?TRZNEJ GRUPY */
//        String host = "localhost";
//        Properties properties = System.getProperties();
//
//        properties.setProperty("mail.smtp.host", host);
//        Session session = Session.getDefaultInstance(properties);

        /* POPRAWIONY KOMPONENT */
        final String username = "wirtualnybank@gmail.com";
        final String password = "1qaz@WSXCV";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        //From - To
        MimeMessage message = new MimeMessage(session);
        message.setFrom((new InternetAddress(senderAddress)));
        message.setSender(new InternetAddress(senderAddress));
        message.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(recipientAddress));

        //Title and body
        message.setSubject(title);
        BodyPart messageBodyPart = new javax.mail.internet.MimeBodyPart();
        messageBodyPart.setText(textOfMessage);
//        MimeBodyPart mail = new javax.mail.internet.MimeBodyPart();
//        try {
//            mail.attachFile(new File("/Users/Jan/Desktop/conversation.xml"));
//        }catch(Exception e){}

        //Creating multipart message
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
//        multipart.addBodyPart(mail);

        //Adding attachments
        if (attachmentList != null && !attachmentList.isEmpty())
        {
            for (AttachmentDetails at : attachmentList)
            {
               // DataSource ds = new FileDataSource(at.fileName);
               // at.attachmentBody.setDataHandler(new DataHandler(ds));

              //  at.attachmentBody.setFileName(at.fileName);
                multipart.addBodyPart(at.attachmentBody);
            }
        }

        //Sending
        message.setContent(multipart);
        return message;
    }

    /**
     * Creates and sends an email.
     *
     * @param senderAddress   - Sender address
     * @param receiverAddress - Receiver address
     * @param title           - Email title
     * @param textOfMessage   - Email content
     * @param attachmentList  - List of email attachments
     */
    @Override
    public void sendEmail(String senderAddress, String receiverAddress, String title, String textOfMessage, List<AttachmentDetails> attachmentList) throws MessagingException
    {
        MimeMessage message;
        try
        {
            message = createEmail(senderAddress, receiverAddress, title, textOfMessage, attachmentList);
            Transport.send(message);

            System.out.println("\n Sucess");
        }
        catch (MessagingException e)
        {
            System.out.println("\n BLad maila " + e);
            e.printStackTrace();
        }
    }

    /**
     * Sends an email.
     *
     * @param message - message to be send
     */
    public void sendEmail(MimeMessage message) throws MessagingException
    {
        try
        {
            Transport.send(message);
        }
        catch (MessagingException e)
        {
            e.printStackTrace();
        }
    }


    public void sendRecoveryLink(String recipientAddress, String senderAddress, String message)
    {

        MimeMessage email = null;
        try
        {
            email = createEmail(senderAddress, recipientAddress, "Odzyskaj hasło", message, null);
            this.sendEmail(email);
        }
        catch (MessagingException e)
        {
            e.printStackTrace();
        }
    }
}
